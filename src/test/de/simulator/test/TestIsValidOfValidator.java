package de.simulator.test;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.simulator.controller.handler.Validator;

/**
 * Pr�ft ob die isValid Methode des Validator korrekt arbeitet.
 * Daf�r werden verschiedenen Testszenarien ausgef�hrt.
 * @author Marcus Meiburg
 * @version 1.0
 */
@RunWith(Parameterized.class)
public class TestIsValidOfValidator {

	private String text;
	private String regEx;

	public TestIsValidOfValidator(String text, String regEx) {
		this.text = text;
		this.regEx = regEx;
	}
	
	/**
	 * Erstellt eine Parameterliste, welche eine Wert
	 * und den dazugeh�rigen regul�ren Ausdruck bereith�lt.
	 * @return Parameterliste.
	 */
	@Parameters(name = "{index}: isValid({0},{1}) = {0}")
	public static Iterable<Object[]> data() {
		return Arrays.asList(new Object[][] {
				{ "0" ,Validator.NATURAL_NUMBER },
				{ "100"  ,Validator.NATURAL_NUMBER_GT0},
				{ "1"  ,Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_1},
				{ "0"  ,Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_1},
				{ "0.123"  ,Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_1},
				{ "0.123"  ,Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_2},
				{ "100.123"  ,Validator.REAL_NUMBER_GT0},
				
				{ "-1" ,Validator.NATURAL_NUMBER },
				{ "0"  ,Validator.NATURAL_NUMBER_GT0},
				{ "1.123"  ,Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_1},
				{ "-0.123"  ,Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_1},
				{ "0"  ,Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_2},
				{ "1"  ,Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_2},
				{ "0"  ,Validator.REAL_NUMBER_GT0}
		});
	}

	
	/**
	 * Pr�ft einen Wert anhand eines regul�ren Ausdruckts und gibt zur�ck
	 * ob der Wert zu dem Ausdruck passt.
	 */
	@Test
	public void isValid_withValidValue_returnsValidValue() {
		double value = Double.parseDouble(this.text);
		
		Assert.assertEquals(Validator.isValid(this.text, this.regEx), value, 0);
	}
}
