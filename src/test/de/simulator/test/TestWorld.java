package de.simulator.test;


import static org.junit.Assert.*;

import org.junit.Test;

import de.simulator.model.World;

/**
 * Testet die World Klasse.
 * @author Marcus Meiburg
 * @version 1.0
 */
public class TestWorld {
	/**
	 * Pr�ft ob 10 Agenten erstellt werden konnten.
	 */
	@Test
	public void createTenAgents() {
		// Arrage
		int numberOfAgents = 10;
		World world = new World(100);
		
		// Act
		world.createAgents(numberOfAgents, 0, 0, 0, 0, 0);
		
		// Assert
		assertEquals(world.getAgents().size(), numberOfAgents);
	}
}
