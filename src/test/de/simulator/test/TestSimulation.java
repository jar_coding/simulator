package de.simulator.test;

import org.junit.Test;

import de.simulator.MainApplication;

/**
 * Simuliert einen Start von der Konsole, es wird kein exaktes Ergebnis erwartet.
 * @author Marcus Meiburg
 *
 */
public class TestSimulation {
	@Test
	public void startSimulationWithParameter() {
		String radius 			= "400.0";
		String numOfAgents 		= "20";
		String numOfTimesteps 	= "100";
		String numOfRepeats 	= "10";
		String numOfInit 		= "10";
		String pm 				= "0.8";
		String pr 				= "0.3";
		String pt 				= "0.1";
		String iRadius 			= "20";
		String stepsize 		= "1";
		
		String[] args = {radius, numOfAgents, numOfTimesteps, numOfRepeats, numOfInit, pm, pr, pt, iRadius, stepsize};
		MainApplication.main(args);
	}
}
