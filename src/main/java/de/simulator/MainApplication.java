package de.simulator;

import java.io.IOException;
import java.util.List;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import de.simulator.controller.MainViewController;
import de.simulator.controller.handler.Validator;
import de.simulator.model.Simulation;
import de.simulator.model.handler.Config;

/**
 * Hauptklasse des Simulators
 * @author Marcus Meiburg
 * @version 1.0
 */
public class MainApplication extends Application {
	
	private static int PARAMETER_SIZE = 10;		// Anzahl der maximalen Parameterzahl.

	public static void main(String[] args) {
		Application.launch(args);
	}
	
	
	@Override
	/**
	 * Haupteinstiegspunkt der JavaFx Anwendung.
	 */
	public void start(Stage stage) {
		final List<String> params = this.getParameters().getRaw();
		if(params.isEmpty()) {
			this.startWithGUI(stage);
		} else if(params.size() >= PARAMETER_SIZE) {
			this.startWithoutGUI(params);
		} else {
			System.out.println("Fehlende Parameter!");
			System.out.println("Nr |          Bezeichnung              |              Format              |");
			System.out.println(" 1 |                   Radius der Welt |                  Reelle Zahl > 0 |");
			System.out.println(" 2 |                Anzahl der Agenten |              Nat. Zahl > 0 < "+ Config.MAX_AGENT_SIZE+" |");
			System.out.println(" 3 |           Anzahl der Zeitschritte |                    Nat. Zahl > 0 |");
			System.out.println(" 4 |         Anzahl der Wiederholungen |                    Nat. Zahl > 0 |");
			System.out.println(" 5 | Anzahl der Initialen Zeitschritte |                    Nat. Zahl > 0 |");
			System.out.println(" 6 |           Wahrsch. einer Bewegung |               Reelle Zahl (0..1) |");
			System.out.println(" 7 |             Wahrsch. zu Verweilen |               Reelle Zahl (0..1) |");
			System.out.println(" 8 |   Wahrsch. die Richtung zu ändern |               Reelle Zahl [0..1] |");
			System.out.println(" 9 |    Interaktionsradius der Agenten |                  Reelle Zahl > 0 |");
			System.out.println("10 |          Schrittweite der Agenten | Reele Zahl > 0 < Radius der Welt |");
			
			System.exit(-1);
		}
	}
	
	
	/**
	 * Startet die Simulation ohne eine Benutzeroberfläche und
	 * benutzt die Parameter der Konsoleneingabe für die Berrechnung.
	 * @param params - Parameter, welche über die Konsole eingegeben worden sind.
	 */
	private void startWithoutGUI(List<String> params) {
		Number[] result = new Number[10];
		if(Validator.isValid(result, params)) {
			Simulation simulation = new Simulation();
			simulation.submitData(
					result[0].doubleValue(),
					result[1].intValue(),
					result[2].intValue(),
					result[3].intValue(),
					result[4].intValue(),
					result[5].doubleValue(),
					result[6].doubleValue(),
					result[7].doubleValue(),
					result[8].doubleValue(),
					result[9].doubleValue()
			);
			simulation.start(true);
		} else {
			System.out.println("Fehler bei der Eingabe der Parameter!");
			System.out.print("Parameter: ");
			System.out.println(params);
			int i = 1;
			for(Number n : result) {
				if(n.equals(-1.0)) {
					System.out.println("Parameter "+i+" ist fehlerhaft!");
				}
				i++;
			}
		}
	}

	
	/**
	 * Startet die Simulation mit einer Benutzeroberfläche.
	 * @param stage Fenster in welchem die Oberfläche dargestellt wird.
	 */
	private void startWithGUI(Stage stage) {
		try {
	        FXMLLoader loader = new FXMLLoader(MainApplication.class.getResource("view/fxml/MainView.fxml"));
	        Parent root;
			root = (Parent) loader.load();
			Scene scene = new Scene(root);
			stage.setResizable(false);
	        stage.setTitle("Simulator");
	        stage.setScene(scene);
	        stage.show();
	        
			MainViewController controller = loader.getController();
	        controller.setStage(stage);
	        controller.setModel(new Simulation());
	        controller.init();
	        
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
