package de.simulator.controller;

import javafx.stage.Stage;
import de.simulator.model.Simulation;

/**
 * Abstrakte Klasse welches die gemeinsamen Attribute und Methoden eines Controllers definiert.
 * @author Marcus Meiburg
 */
public abstract class Controller {
	
	protected Simulation model;	// Model des Controllers
	protected Stage stage;		// Fenster des Controllers.
	
	/**
	 * Setzt das Model und initialiesiert den Controller.
	 * @param model Model welches gesetzt wird.
	 */
	public void setModel(Simulation model) {
		this.model = model;
	}
	
	/**
	 * Übergibt dem Controller sein zugehöriges Fenster.
	 * @param stage Fenster des Controllers.
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	
	/**
	 * Initialiesiert den Controller.
	 */
	public void init() {}
}
