package de.simulator.controller.handler;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Representation der Daten eines Agenten für Validierungszwecke in einer Tabelle.
 * 
 * Diese Klasse ist speziell für die Validierung, und die Interaktion des Nutzters mit der Oberfläche.
 * Da explizit Fehleingaben erlaubt sind, der Nutzer dann aber darauf hingewiesen werden soll, muss
 * es einer Tabelle erlaubt sein, jedwede Strings zuzulassen.
 * 
 * Wenn der Nutzer eine Eingabe in einer Zelle beendet, wird diese an die ValidationAgentHelper Klasse übergeben und gespeichert.
 * Anhand von späteren Validierungen kann der Nutzer auf einene Fehler hingewiesen werden.
 * Sobald es keine Fehler mehr in der ValidationAgentHelper Klasse gibt, werden die Daten anhand der ID dem verbundenen Agenten übergeben
 * und die Simulation kann gestartet werden.
 * 
 * @author Marcus Meiburg
 * @version 1.0
 */
public class ValidationAgentHelper {

	private final StringProperty id;				// id des Agenten
	private final StringProperty pm;				// Bewegungswahrscheinlichkeit
	private final StringProperty pr;				// Verweilwahrscheinlichkeit
	private final StringProperty pt;				// Richtungsänderungswahrscheinlichkeit
	private final StringProperty stepSize;			// Schrittweite
	private final StringProperty interactionRadius;	// Interaktionsradius
	
	// Initialisierungsblock der privaten Attribute
	{
		this.id = new SimpleStringProperty();
		this.pm = new SimpleStringProperty();
		this.pr = new SimpleStringProperty();
		this.pt = new SimpleStringProperty();
		this.stepSize = new SimpleStringProperty();
		this.interactionRadius = new SimpleStringProperty();
	}
	
	
	/**
	 * Variable welche die ID angibt.
	 * @return ID des Agenten.
	 */
	public StringProperty idProperty() {
		return this.id;
	}
	
	
	/**
	 * Variable welche die voreingestellte Bewegungswahrscheinlichkeit angibt.
	 * @return eingestellte Bewegungswahrscheinlichkeit.
	 */
	public StringProperty pmProperty() {
		return this.pm;
	}
	
	
	/**
	 * Variable welche die voreingestellte Verweilwahrscheinlichkeit angibt.
	 * @return eingestellte Verweilwahrscheinlichkeit.
	 */
	public StringProperty prProperty() {
		return this.pr;
	}
	
	
	/**
	 * Variable welche die voreingestellte Richtungsänderungswahrscheinlichkeit angibt.
	 * @return eingestellte Bewegungswahrscheinlichkeit.
	 */
	public StringProperty ptProperty() {
		return this.pt;
	}
	
	
	/**
	 * Variable welche die Schrittweite angibt.
	 * @return Schrittweite des Agenten.
	 */
	public StringProperty stepSizeProperty() {
		return this.stepSize;
	}
	
	
	/**
	 * Variable welche den Interaktionsradius angibt.
	 * @return Interaktionsradius des Agenten.
	 */
	public StringProperty interactionRadiusProperty() {
		return this.interactionRadius;
	}
}
