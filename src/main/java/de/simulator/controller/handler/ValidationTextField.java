package de.simulator.controller.handler;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextField;

/**
 * Die Klasse ValidationTextField, erweitert das normale TextFeld um die Eigenschaften einer Fehlererkennung.
 * F�r das Erkennen von Fehlern, wird dem TextFeld ein regul�rer Ausdruck �bergeben, welcher bei der Eingabe
 * von Zeichen durch den Validator abgefragt wird.
 * 
 * Zus�tzlich bietet die Klasse die M�glichkeit ein Limit zu pr�fen.
 * Durch eine Visualiesierung wird der Nutzer auf den Fehler hingewiesen.
 * 
 * Da der regul�re Ausdruck und das Limit im FXML bestimmt werden kann, m�ssen die Attribute als Properties vorliegen.
 * 
 * @author Marcus Meiburg
 * @version 1.0
 */
public class ValidationTextField extends TextField {
	private double value;
	private BooleanProperty valid;								// Pr�ft ob die Zelle g�ltig ist.
	private StringProperty regEx;									// Regul�rer Ausdruck, welcher erf�llt sein muss.
	private DoubleProperty limit;									// Limit, welche nicht �berschritten werden darf.
	private ObjectProperty<ValidationTextField> dependentField;	// Referenz auf ein abh�ngiges Feld
	
	// Initialisierungsblock der privaten Attribute
	{
		this.valid = new SimpleBooleanProperty(true);
		this.regEx = new SimpleStringProperty();
		this.limit = new SimpleDoubleProperty(-1);
		this.dependentField = new SimpleObjectProperty<ValidationTextField>();
	}
	
	
	/**
	 * Default Konstruktor
	 * Wird ben�tigt um den Zugriff aus der FXML Datei auf die Klasse zu gew�hrleisten.
	 */
	public ValidationTextField() {}
	
	
	/**
	 * Konstruktor, welcher einen regul�ren Ausdruck zum Pr�fen von Fehleingaben �bergibt.
	 * @param regEx regul�rer Ausdruck
	 */
	public ValidationTextField(String regEx) {
		this.regEx.set(regEx);
		this.getStyleClass().add("validation-textfield");
	}
	
	
	/**
	 * Konstruktor, welcher einen regul�ren Ausdruck und ein Limit zum Pr�fen von Fehleingaben �bergibt.
	 * @param regEx regul�rer Ausdruck
	 * @param limit limit, welches nicht �berschritten werden darf.
	 */
	public ValidationTextField(String regEx, double limit) {
		this(regEx);
		this.limit.set(limit);
	}
	
	
	/**
	 * Gibt das abh�ngige Feld zur�ck.
	 * @return abh�ngiges Feld
	 */
	public final ValidationTextField getDependentField() {
		return this.dependentField.get();
	}
	
	
	/**
	 * Setzt das abh�ngige Feld
	 * @param field abh�ngiges Feld
	 */
	public void setDependentField(ValidationTextField field) {
		this.dependentField.set(field);
	}
	
	
	/**
	 * Variable, welche das abh�ngige Feld behandelt.
	 * @return abh�ngiges Feld
	 */
	public ObjectProperty<ValidationTextField> dependentFieldProperty() {
		return this.dependentField;
	}
	
	
	/**
	 * Gibt das Limit zur�ck
	 * @return Limit, welches nicht �berschritten werdend darf.
	 */
	public final double getLimit() {
		return this.limit.get();
	}
	
	
	/**
	 * Setzt das Limit
	 * @param limit Wert auf welches, das Limit gesetzt wird.
	 */
	public void setLimit(double limit) {
		this.limit.set(limit);
	}
	
	
	/**
	 * Variable, welche das Limit behandelt.
	 * @return Limit
	 */
	public ReadOnlyDoubleProperty limitProperty() {
		return this.limit;
	}
	
	
	/**
	 * Gibt den regul�ren Ausdruck zur�ck
	 * @return regul�rere Ausdruck
	 */
	public final String getRegEx() {
		return this.regEx.get();
	}
	
	
	/**
	 * Setzt den regul�ren Ausdruck
	 * @param regEx Wert auf welches der Ausdruck gesetzt wird.
	 */
	public void setRegEx(String regEx) {
		this.regEx.set(regEx);
	}
	
	
	/**
	 * Variable, welche den regul�ren Ausdruck behandelt.
	 * @return regul�rer Ausdruck
	 */
	public ReadOnlyStringProperty regExProperty() {
		return this.regEx;
	}

	
    /**
	 * Gibt den aktuellen Wert des Textfeldes zur�ck.
	 * -1 Wenn der Wert ung�ltig ist.
	 * @return Wert des Textfeldes
	 */
	public Number getValue() {
		return this.value;
	}


	/**
     * Gibt zur�ck ob das Textfeld sich in einem g�ltigen Zustand befindet.
     * @return true, wenn es g�ltig ist, sonst false.
     */
    public final boolean isValid() {
    	this.validate();
    	return this.valid.get();
    }

    
    /**
	 * Setzt die aktuelle g�ltigkeit des Feldes.
	 * @param flag Wert auf welches gesetzt werden soll.
	 */
	private void setValid(boolean flag) {
		this.pseudoClassStateChanged(Validator.ERROR, !flag);
		this.valid.set(flag);
	}


	/**
	 * Variable, welche den G�ltigkeitszustand behandelt.
	 * @return G�ltigkeitszustand
	 */
	public ReadOnlyBooleanProperty validProperty() {
		return this.valid;
	}


	/**
	 * Pr�ft ob das Limit nicht �berschritten wurde und anhand des
	 * regul�ren Ausdrucks ob der Inhalt des Textfeldes g�ltig ist.
	 */
	public void validate() {
		double limit = 0;
		try {
			limit = this.limit.get();
		} catch(NumberFormatException e) {
			limit = -1;
		}
		
		this.value = Validator.isValid(this.getText(), this.regEx.get(), limit);
		this.setValid(this.value != -1);
	
		if(this.dependentField.get() != null) {
			this.dependentField.get().validate();
		}
	}

	
	@Override
	/**
	 * Ersetzt den aktuellen Text und pr�ft diesen.
	 */
	public void replaceText(int start, int end, String text) {
	    super.replaceText(start, end, text);
	    this.validate();
	}

	
	@Override
	/**
	 * Ersetzt den aktuell ausgew�hlten Text und pr�ft diesen.
	 */
	public void replaceSelection(String text) {
	    super.replaceSelection(text);
	    this.validate();
	}
}
