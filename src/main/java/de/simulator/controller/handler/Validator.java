package de.simulator.controller.handler;

import java.util.List;

import javafx.css.PseudoClass;
import de.simulator.model.handler.Config;

/**
 * Der Validator bietet statische Methoden an um einen String anhand eine regul�ren Ausdrucks zu pr�fen.
 * So k�nnen Eingaben validiert und Fehler fr�hzeitig abgefangen werden.
 * @author Marcus Meiburg
 * @version 1.0
 */
public abstract class Validator {
	
	public static final PseudoClass ERROR = PseudoClass.getPseudoClass("error");
	public static final PseudoClass VALID = PseudoClass.getPseudoClass("valid");
	//inculded
	public static final String REAL_NUMBER_BETWEEN_ZERO_AND_ONE_1 = "^((0(\\.[0-9]+)?)|1(\\.0)?)$"; // Intervall [0 - 1]
	//excluded
	public static final String REAL_NUMBER_BETWEEN_ZERO_AND_ONE_2 = "^((0\\.)(?=\\d*[1-9])\\d+)$";  // Intervall (0 - 1)
	public static final String NATURAL_NUMBER = "^\\d+$";
	public static final String NATURAL_NUMBER_GT0 = "^([1-9]\\d*)$";
	public static final String REAL_NUMBER_GT0 = "^([1-9]\\d*\\.?\\d*)$|^((0\\.)(?=\\d*[1-9])\\d+)$";
	
	
	/**
	 * Pr�ft den String anhand des �bergebenen regul�ren Ausdrucks. 
	 * @param text - �bergebener Text.
	 * @param regEx - regul�rer Ausdruck.
	 * @return true, wenn der String g�ltig ist, sonst false.
	 */
	private static boolean matches(String text, String regEx) {
		if(text.matches(regEx)) {
			return true;
		}
		return false;
	}

	
	/**
	 * Pr�ft den String anhand des �bergebenen regul�ren Ausdrucks,
	 * wenn der Wert g�ltig ist und sich im �bergebenen Limit befindet, wird dieser zur�ckgegeben.
	 * Andernfalls wird -1 zur�ckgegeben. 
	 * @param text - �bergebener Text.
	 * @param regEx - regul�rer Ausdruck.
	 * @param limit - Limit, welches der Wert nicht �berschreiten darf
	 * @return -1 wenn der Wert ung�ltig ist, sonst den konvertierten String als double
	 */
	public static double isValid(String text, String regEx, double limit) {
		double value = -1;
		if(matches(text, regEx)) {
			value = Double.parseDouble(text);
			if(limit != -1) {
				value = value < limit ? value : -1;
			}
		}			
		return value;
	}

	
	/**
	 * Pr�ft den String anhand des �bergebenen regul�ren Ausdrucks,
	 * wenn der Wert g�ltig ist, wird dieser zur�ckgegeben.
	 * Andernfalls wird -1 zur�ckgegeben. 
	 * @param text - �bergebener Text.
	 * @param regEx - regul�rer Ausdruck.
	 * @return -1 wenn der Wert ung�ltig ist, sonst den konvertierten String als double
	 */
	public static double isValid(String text, String regEx) {
		double value = -1;
		if(matches(text, regEx)) {
			value = Double.parseDouble(text);
		}
		return value;
	}
	
	
	/**
	 * Pr�ft die Liste der �ber die Konsole eingegebenen Parameter und speichert
	 * die Ergebnisse der Pr�gun in das �bergebene Ergebnisarray.
	 * @param result - Array mit den Ergebnissen der Pr�fung der einzelnen Parametern.
	 * @param params - Eingegebene Parameter �ber die Konsole.
	 * @return true falls die Parameter g�ltig sind, sonst false.
	 */
	public static boolean isValid(Number[] result, List<String> params) {
		boolean valid = true;
		
		// Speichert die gepr�ten Werte im Ergebnisarray.
		result[0] = isValid(params.get(0), Validator.REAL_NUMBER_GT0);
		result[1] = isValid(params.get(1), Validator.NATURAL_NUMBER_GT0, Config.MAX_AGENT_SIZE);
		result[2] = isValid(params.get(2), Validator.NATURAL_NUMBER_GT0);
		result[3] = isValid(params.get(3), Validator.NATURAL_NUMBER_GT0);
		result[4] = isValid(params.get(4), Validator.NATURAL_NUMBER_GT0);
		result[5] = isValid(params.get(5), Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_2);
		result[6] = isValid(params.get(6), Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_2);
		result[7] = isValid(params.get(7), Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_1);
		result[8] = isValid(params.get(8), Validator.REAL_NUMBER_GT0);
		result[9] = isValid(params.get(9), Validator.REAL_NUMBER_GT0, result[0].doubleValue());

		// Pr�ft im Ergebnisarray ob alle Werte g�ltig sind. (-1.0 = ung�ltig)
		for(Number n : result) {
			valid &= n.equals(-1.0) ? false : true;
		}
		return valid;
	}
}
