package de.simulator.controller.handler;

import java.util.ArrayList;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

/**
 * Die ValidationCellFactory formatiert eine Spalte in einer Tablle so, das die einzelnen Zellen
 * validiert werden k�nnen. Dabei wird anhand eine regul�ren Ausdrucks gepr�ft ob der Inhalt der Zelle
 * korrekt ist. Weiterhin kann ein Limit f�r eine Zelle angegeben werden, welches nicht �berschritten werden darft.
 * 
 * @author Marcus Meiburg
 * @version 1.0
 *
 */
public class ValidationCellFactory implements Callback<TableColumn<ValidationAgentHelper, String>, TableCell<ValidationAgentHelper, String>>  {
	
	private ArrayList<ValidationCell> cells;
	private String regEx;
	private double limit;
	
	// Initialisierungsblock der privaten Attribute.
	{
		this.cells = new ArrayList<>();
		this.limit = -1;
	}
	
	
	/**
	 * Konstruktor welcher einen regul�ren Ausdruck f�r die Validierung �bergibt.
	 * @param regEx regul�rer Ausdruck
	 */
	public ValidationCellFactory(String regEx) {
		this.regEx = regEx;
	}
	
	
	/**
	 * Konstruktor welcher einen regul�ren Ausdruck und ein Limit f�r die Validierung �bergibt.
	 * @param regEx regul�rer Ausdruck
	 * @param limit Limit welches nicht �berschritten werden darf.
	 */
	public ValidationCellFactory(String regEx, double limit) {
		this(regEx);
		this.limit = limit;
	}
	
	
	@Override
	/**
	 * Methode, welche beim erstellen einer Zelle aufgerufen wird.
	 */
	public TableCell<ValidationAgentHelper, String> call(TableColumn<ValidationAgentHelper, String> param) {
		ValidationCell cell = new ValidationCell(this.regEx, this.limit);
		this.cells.add(cell);
		return cell;
	}
	
	
	/**
	 * Pr�ft ob alle Zellen der Tabelle g�ltig sind.
	 * @return true, wenn sie g�ltig sind, sonst false.
	 */
	public boolean isValid() {
		boolean flag = true;
		for(ValidationCell c : this.cells) {
			flag &= c.isValid();
		}
		return flag;
	}
}
