package de.simulator.controller.handler;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 * Der DialogHandler h�lt einige standartisierte JavaFX Dialoge bereit.
 * @author Marcus Meiburg
 * @version 1.0
 */
public abstract class DialogHandler {
	

	/**
	 * Zeigt einen Dialog an, welcher anhand der Parameter individualisiert werden kann.
	 * @param type - Dialog Typ (Info, Warning, ...)
	 * @param title - Titel des Dialoges.
	 * @param header - Header, welcher angezeigt werden soll.
	 * @param text - Text, der Angezeigt werden soll.
	 * @param options - Falls es eine Auswahl gibt, k�nnen hier die variablen Parameter �bergeben werden.
	 * @return true, wenn die erste Option gedr�ckt wurde, sonst false.
	 */
	public static boolean show(AlertType type, String title, String header, String text, ButtonType ...options) {
		Alert alert = new Alert(type, text, options);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.showAndWait();

		if(options.length > 0) {
			if(alert.getResult() == options[0]) {
				return true;
			} else if(alert.getResult() == options[1]) {
				return false;
			}	
		}
		return false;
	}
	
	
	/**
	 * Zeigt an das falsche Eingaben get�tigt worden sind.
	 * Zeigt eine Warnung an.
	 */
	public static void showInvalidInputError() {
		DialogHandler.show(
				AlertType.ERROR,
				"Fehler!",
				"Ihre Eingaben sind fehlerhaft!",
				"Pr�fen Sie die Eingaben erneut.");
	}

	
	/**
	 * Fragt ob die Anwendung beendet werden soll.
	 * Pr�ft ob die Warnung mit dem Ok Button best�tigt wurde.
	 * @return true wenn Ok gedr�ckt wurde, sonst false
	 */
	public static boolean showCloseWarning() {
		return DialogHandler.show(
				AlertType.CONFIRMATION,
				"Warnung!",
				"Die Anwendung wird beendet.",
				"Wirklich fortfahren?",
				ButtonType.YES,
				ButtonType.NO);
	}
}
