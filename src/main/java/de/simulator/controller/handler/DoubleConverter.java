package de.simulator.controller.handler;

import java.math.BigDecimal;

import javafx.util.StringConverter;

/**
 * Helferklasse welche Versucht einen String in einen Double zu casten.
 */
public class DoubleConverter extends StringConverter<Number> {

	
	@Override
	/**
	 * Von String in Double parsen.
	 */
	public Number fromString(String text) {
		double value = 0;
		try {
			value = Double.valueOf(text);
		} catch(NumberFormatException e) { 
			// leer
		}
		return value;
	}

	
	@Override
	/**
	 * Von einer Number in ein String parsen.
	 * Hier wird BigDecimal verwendet um bei der Ausgabe auf dem Bildschirm
	 * keine E-Notation zu erhalten. So ist die Validierung mit einem regul�ren
	 * Ausdruck einfacher.
	 */
	public String toString(Number number) {
		BigDecimal value = new BigDecimal(String.valueOf(number));
		return value.toPlainString();
	}
}
