package de.simulator.controller.handler;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Die Klasse ValidationCell, erweitert die normale TableCell um die Eigenschaften einer Fehlererkennung.
 * F�r das Erkennen von Fehlern, wird dem TextFeld ein regul�rer Ausdruck �bergeben, welcher bei der Eingabe
 * von Zeichen durch den Validator abgefragt wird.
 * 
 * Zus�tzlich bietet die Klasse die M�glichkeit ein Limit zu pr�fen.
 * 
 * Durch eine Visualiesierung wird der Nutzer auf den Fehler hingewiesen.
 * 
 * @author Marcus Meiburg
 * @version 1.0
 */
public class ValidationCell extends TableCell<ValidationAgentHelper, String> {

	private ValidationTextField textField;		// Textfeld, welche zum Editieren der Zelle ben�tigt wird.
	private BooleanProperty valid;				// Pr�ft ob die Zelle g�ltig ist.
	private String regEx;						// Regul�rer Ausdruck, welcher erf�llt sein muss.
	private double limit;						// Limit, welche nicht �berschritten werden darf.
	
	// Initialisierungsblock der privaten Variablen
	{
		this.valid = new SimpleBooleanProperty(true);
	}
	
	
	/**
	 * Konstruktor, welcher den regul�ren Ausdruck und das Limit �bergibt.
	 * @param regEx regul�rer Ausdruck
	 * @param limit Limit, welches nicht �berschritten werden darf.
	 */
	public ValidationCell(String regEx, double limit) {
		this.regEx = regEx;
		this.limit = limit;
		this.getStyleClass().add("validation-table-cell");
	}

	
	/**
	 * Initialiesiert das Textfeld, welche ben�tigt wird, wenn die Zelle
	 * editierbar ist.
	 */
    private void initTextField() {
		this.textField = new ValidationTextField(this.regEx, this.limit);
	    this.textField.setText(this.getString());
	    this.textField.validate();
	    
	    this.textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
	        public void handle(KeyEvent t) {
	            if (t.getCode() == KeyCode.ENTER) {
	            	commitEdit(textField.getText());
	            } else if (t.getCode() == KeyCode.ESCAPE) {
	            	cancelEdit();
	            }
	        }
	    });
	    
	    this.textField.focusedProperty().addListener((obs, wasFocused, isNowFocused) -> {
	        if (! isNowFocused) {
	        	this.cancelEdit();
	        }
	    });
	}

    
	@Override
	/**
	 * Wird beim beginnen des Editierens einer Zelle aufgerufen.
	 */
    public void startEdit() {
        if (!this.isEmpty()) {
            super.startEdit();
            this.initTextField();
            this.setText(null);
            this.setGraphic(textField);
            this.textField.requestFocus();
        }
    }
    
	
    @Override
    /**
     * Wird aufgerufen wenn das Editieren abgebrochen wird.
     */
    public void cancelEdit() {
        super.cancelEdit();

        this.setText((String) getItem());
        this.setGraphic(null);
    }
    
    
    @Override
    /**
     * Wird beim erstellen einer Zelle aufgerufen.
     */
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            this.setText(null);
            this.setGraphic(null);
        } else {
            if (this.isEditing()) {
                if (this.textField != null) {
                	this.textField.setText(this.getString());
                }
                this.setText(null);
                this.setGraphic(this.textField);
            } else {
            	this.setText(this.getString());
                this.setGraphic(null);
            }
            this.validate();
        }
    }

    
    /**
     * Gibt den aktuellen Inhalt der Zelle zur�ck,
     * falls der Inhalt leer ist (NULL) wird ein leerer String zur�ckgegeben
     * um Fehler zu vermeiden.
     * @return aktuller String der Zelle.
     */
	private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
    
	
	/**
    * Gibt zur�ck ob die Zelle sich in einem g�ltigen Zustand befindet.
    * @return true, wenn es g�ltig ist, sonst false.
    */
	public final boolean isValid() {
		return this.valid.get();
	}

	
	/**
	 * Pr�ft ob das Limit nicht �berschritten wurde und anhand des
	 * regul�ren Ausdrucks ob der Inhalt der Zelle g�ltig ist.
	 */
    private void validate() {
    	double value = Validator.isValid(this.getText(), this.regEx);
    	this.valid.set(value != -1);
    	this.pseudoClassStateChanged(Validator.ERROR, !this.valid.get());
    	this.pseudoClassStateChanged(Validator.VALID, this.valid.get());
    }
}
