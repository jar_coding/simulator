package de.simulator.controller;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import de.simulator.controller.handler.*;
import de.simulator.model.Agent;

/**
 * Steuert die Eingaben, die ein Nutzer im zweiten Konfigurationsfester vornehmen kann.
 * Dort k�nnen die Eigenschaften einzelner Agenten vorgenommen werden.
 * @author Marcus Meiburg
 * @version 1.0
 */
public class ConfigView2Controller extends Controller {
	
    @FXML private Label fxWorldRadius;
    // Agentenparameter in der Tabelle
    @FXML private TableView<ValidationAgentHelper> fxParameters;
    @FXML private TableColumn<ValidationAgentHelper, Integer> 	fxParametersId;
    @FXML private TableColumn<ValidationAgentHelper, String> 	fxParametersPm;
    @FXML private TableColumn<ValidationAgentHelper, String> 	fxParametersPr;
    @FXML private TableColumn<ValidationAgentHelper, String> 	fxParametersPt;
    @FXML private TableColumn<ValidationAgentHelper, String> 	fxParametersRadius;
    @FXML private TableColumn<ValidationAgentHelper, String> 	fxParametersStep;

    private ValidationCellFactory p1Factory;
    private ValidationCellFactory p2Factory;
    private ValidationCellFactory radiusFactory;
    private ValidationCellFactory stepSizeFactory;
    
    @Override
    public void init() {
    	// Einzelne Zellen k�nnen selektiert werden.
    	this.fxParameters.getSelectionModel().setCellSelectionEnabled(true);
    }
    
    
    /**
     * Bereitet die ParameterTabelle f�r die Validierung vor.
     */
    private void setupTable() {
    	this.fxParametersId.setCellValueFactory(new PropertyValueFactory<ValidationAgentHelper, Integer>("id"));
    	this.fxParametersPm.setCellValueFactory(new PropertyValueFactory<ValidationAgentHelper, String>("pm"));
    	this.fxParametersPr.setCellValueFactory(new PropertyValueFactory<ValidationAgentHelper, String>("pr"));
    	this.fxParametersPt.setCellValueFactory(new PropertyValueFactory<ValidationAgentHelper, String>("pt"));
    	this.fxParametersRadius.setCellValueFactory(new PropertyValueFactory<ValidationAgentHelper, String>("interactionRadius"));
    	this.fxParametersStep.setCellValueFactory(new PropertyValueFactory<ValidationAgentHelper, String>("stepSize"));
        
    	this.p1Factory = new ValidationCellFactory(Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_1);
    	this.p2Factory = new ValidationCellFactory(Validator.REAL_NUMBER_BETWEEN_ZERO_AND_ONE_2);
    	this.stepSizeFactory = new ValidationCellFactory(Validator.REAL_NUMBER_GT0, this.model.getWorld().getRadius());
    	this.radiusFactory = new ValidationCellFactory(Validator.REAL_NUMBER_GT0);
    	
    	this.fxParametersPm.setCellFactory(this.p2Factory);
    	this.fxParametersPr.setCellFactory(this.p2Factory);
    	this.fxParametersPt.setCellFactory(this.p1Factory);
    	this.fxParametersRadius.setCellFactory(this.radiusFactory);
    	this.fxParametersStep.setCellFactory(this.stepSizeFactory);
    }

    
    /**
     * Verarbeitet die vom Model kommenden Daten.
     * Die Agenten des Models werden mit dm ValidationAgentHelper verbunden.
     * Zus�tzlich wird die Parametertabelle vorbereitet.
     */
    public void processData() {
    	this.fxParameters.getItems().clear();
    	this.fxWorldRadius.setText(String.valueOf(this.model.getWorld().getRadius()));

    	for(Agent a : this.model.getWorld().getAgents()) {
    		ValidationAgentHelper fxAgent = new ValidationAgentHelper();
    		fxAgent.idProperty().set(String.valueOf(a.getId()));
    		Bindings.bindBidirectional(fxAgent.pmProperty(), a.pmProperty(), new DoubleConverter());
    		Bindings.bindBidirectional(fxAgent.prProperty(), a.prProperty(), new DoubleConverter());
    		Bindings.bindBidirectional(fxAgent.ptProperty(), a.ptProperty(), new DoubleConverter());
    		Bindings.bindBidirectional(fxAgent.interactionRadiusProperty(), a.interactionRadiusProperty(), new DoubleConverter());
    		Bindings.bindBidirectional(fxAgent.stepSizeProperty(), a.stepSizeProperty(), new DoubleConverter());
    		this.fxParameters.getItems().add(fxAgent);
    	}
    	this.setupTable();
    }
    
    
	/**
	 * Pr�ft ob die Textfelder alle g�ltige Werte beinhalten.
	 * @return true, wenn alle Textfelder g�ltig sind, sonst false
	 */
    public boolean isValidInput() {
    	return true & this.p1Factory.isValid() & this.p2Factory.isValid() &
    			this.radiusFactory.isValid() & this.stepSizeFactory.isValid();
    }

    
	/**
	 * Anders als beim ersten Konfigurationsfenster, wurden die Daten schon mittels Binding an
	 * das Model �bergeben und gleichzeitig gepr�ft.
	 * Da der ValidationAgentHelper auch falsche Eingaben speichert, wird gepr�ft ob noch falsche
	 * Eigaben vorliegen. Falls das nicht der Fall ist, kann die Simulation gestartet werden.
	 * @return true, wenn die Eingaben �bergeben werden konnten, sonst false.
	 */
	public boolean sendDataToModel() {
		if(this.isValidInput()) {
			return true;
		}
		DialogHandler.showInvalidInputError();
		return false;
	}
}
