package de.simulator.controller;

import java.io.IOException;

import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import de.simulator.MainApplication;
import de.simulator.controller.handler.DialogHandler;
import de.simulator.model.Agent;
import de.simulator.model.handler.Config;
import de.simulator.model.handler.DataHandler;
import de.simulator.view.Renderer;

/**
 * Der MainView Controller, steuert die Eingaben und Ausgaben im Hauptfenster des Simulators.
 * @author Marcus Meiburg
 * @version 1.0
 */
public class MainViewController extends Controller {
	// ###### Hauptfenster ######
	@FXML private VBox fxMainView;								// Wurzel Element des Hauptfensters.
	@FXML private VBox fxLeftMainView;							// Linker Teil des Hauptfensters.
	@FXML private VBox fxRightMainView;							// Rechter Teil des Hauptfensters.
	@FXML private VBox fxProgress;								// Element des Fortschrittsbalkens und des "Bitte Warten" Textes.
    @FXML private ProgressBar fxProgressBar;					// Fortschrittsbalken.
	// Agentenparameter
	@FXML private TableView<Agent> fxParametersTableView;		// Tabelle in denen die Parameter der Agenten angezeigt werden.
	
	// Simulation
	@FXML private Label fxMainViewWorldRadius;					// Label auf welchem der Radius der Welt angezeigt wird.
	@FXML private Label fxTimesteps;							// Label auf welchem die aktuellen Zeitschritte angezeigt werden.
	@FXML private Label fxRepeats;								// Label auf welchem die aktuellen Wiederholungen angezeigt werden.

    @FXML private Label fxMaxTimesteps;
    @FXML private Label fxMaxRepeats;

	@FXML private Slider fxSimulationRate;						// Slider, welcher die Geschwindigkeit der Simulation steuert.
	@FXML private BarChart<Number, String> fxInteractionChart;	// Diagramm, welches die H�ufigkeiten der Interaktionen der Agenten anzeigt.

    @FXML private Pane fxWorldPane;								// Container, welche die Agenten auf dem Bildschirm anzeigt.
    @FXML private Pane fxPositionDataPane;						// Container, welcher die Positionsdaten der Agenten anzeigt.
    @FXML private StackPane fxContainerRight;
    
    @FXML private CheckMenuItem fxMenuPositionData;				// Men�eintrag, welches es erlaubt die Positionsdaten anzuzeigen.
    @FXML private CheckMenuItem fxMenuInteractionRadius;
    @FXML private ImageView fxColorImage;						// Bild, welches die verschiedenen H�ufigkeiten der Positionsdaten repr�sentiert.
    @FXML private Label fxMaxColorLabel;						// Label auf dem die maximale H�ufigkeit der Positionsdaten angezeigt werden.
    @FXML private HBox fxColorImageContainer;					// Container, welcher die Reichweite der Positionsdatenh�ufigkeiten anzeigt.
	@FXML private Text fxMessage;								// Nachricht, welche die Start- und Endnachricht enth�lt.
	@FXML private Circle fxInteractionCircle; 					// Bestimmt die maximale Gr��e des Interaktionsradius
	
	private Agent selectedAgent;								// Refernez auf den derzeitig selektierten Agenten.
	private Circle interactionRadius = new Circle();			// Interaktionsradius des derzeit selektierten Agenten.
	private Stage configStage;									// Referenz auf das Konfigurationsfenster
	private Paint lastAgentColor;								// Farbe des zuletzt ausgew�hlten Agentens.
	
	private Circle[] fxAgents;									// Array der Agenten Elemente, welche im SceneGraphe dargestellt werden.
	private SVGPath[][] fxSegments;								// Array der Sektor Elemente, welche im SceneGraphe dargestellt werden.
	
	private final Timeline timer = new Timeline();				// Zeitdiskreter Timer.
	
	
	@Override
	/**
	 * Initialisiert den Hauptcontroller.
	 * Und erstellt das Konfigurationsfenster
	 * Setzt die Bindinungs und Listener.
	 */
	public void init() {
		// Entfernen aller nicht ben�tigten SceneGraph Elemente
		this.removeFromSceneGraph(
				this.fxWorldPane,
				this.fxPositionDataPane,
				this.fxInteractionCircle,
				this.fxMessage,
				this.fxProgress
		);
		
		// Erstellung der Konfigurationssicht.
	    this.createConfigurationStage();
	    // Initialisierung der Bindings.
		this.fxMaxColorLabel.textProperty().bind(this.model.maxTimestepsProperty().asString());
		this.fxSimulationRate.valueProperty().set(Config.START_RATE_OF_SIMULATION);
		// Initialisierung der Listener.
	    this.initListener();
	}
	
	
	/**
	 * Erstellt Listender, welche auf eintretene Ereignisse reagieren.
	 */
	private void initListener() {
		// Wenn das Hauptfenster geschlossen wird, wird auch das Konfigurationsfenster
		// geschlossen und die Anwendung beendet.
		this.stage.setOnCloseRequest(e -> {
			if(DialogHandler.showCloseWarning()) {
				this.model.stop();
				this.stage.close();
				this.configStage.close();
			} else {
				e.consume();
			}
		});
		
		// Aktualiesiert jeden Zeitschritt die SceneGraph Elemente.
		this.model.currentTimestepProperty().addListener(e -> this.update());
		
		// Aktualiesiert die Timer Geschwindigkeit abh�ngig von der Slider position.
		this.fxSimulationRate.valueProperty().addListener((obs, oldValue, newValue) -> {
			// Wenn der Slider auf 0 steht, wird der Timer gestopt.
			if(newValue.equals(0.0)) {
				this.timer.stop();
			} else if(!newValue.equals(0.0) && oldValue.equals(0.0)) {
				this.timer.play();
			}
			this.timer.rateProperty().set(Math.sqrt(newValue.doubleValue()));
		});
		
		// Reaktion auf das Einschalten der Positionsdaten.
		this.fxMenuPositionData.selectedProperty().addListener(e -> {
			if(this.selectedAgent != null) {
				this.showPositionData();
			}
		});
		
		// Reaktion auf das Beenden der Simulation
		this.model.finishedProperty().addListener((obs, oldValue, newValue) -> {
			this.fxMessage.setOpacity(0);
			if(newValue) {
				// Daten am Ende der Simulation anzeigen.
				this.handleSimulationFinish();
			}
		});
		
		// Reaktion auf das selektieren eines Agenten in der Parameter Tabelle
		this.fxParametersTableView.getSelectionModel().selectedItemProperty().addListener((obs, lastAgent, selectedAgent) -> {
			if(lastAgent != null) {
				if(!this.model.isFinished()) {
					Circle circle = this.fxAgents[lastAgent.getId() - 1];
					if(circle != null) {
						circle.setFill(this.lastAgentColor);
					}
				}
			}
			if(selectedAgent != null) {
				this.selectedAgent = selectedAgent;
				if(!this.model.isFinished()) {
					this.handleSelectedAgent();
					this.handleInteractionRadius();
				}
				this.showInteractionData();
				this.showPositionData();
			}
		});
		
		// Reaktion auf das Ausw�hlen der Ansicht f�r die Positionsdaten.
		this.fxMenuPositionData.selectedProperty().addListener((obs, oldValue, newValue) -> {
			if(newValue) {
				this.addToSceneGraph(this.fxPositionDataPane);
				this.fxPositionDataPane.toBack();
			} else {
				this.removeFromSceneGraph(this.fxPositionDataPane);
			}
			this.fxColorImageContainer.setVisible(newValue);
		});
		
		// Reaktion auf das Ausw�hlen der Ansicht f�r den Interaktionsradius.
		this.fxMenuInteractionRadius.selectedProperty().addListener((obs, oldValue, newValue) -> {
			if(newValue) {
				this.addToSceneGraph(this.fxInteractionCircle);
				this.handleInteractionRadius();
			} else {
				this.removeFromSceneGraph(this.fxInteractionCircle);
			}
		});
	}


	/**
	 * Erstellt das Konfigurationsfenster und setzt Listener, die auf das Schlie�en
	 * und das erneute �ffnen des Fensters reagieren.
	 */
	private void createConfigurationStage() {
		try {
			FXMLLoader loader = new FXMLLoader(MainApplication.class.getResource("view/fxml/MainConfigView.fxml"));
			Parent root = (Parent) loader.load();
			this.configStage = new Stage();
			this.configStage.setTitle("Konfiguration");
			this.configStage.initOwner(this.stage);
			this.configStage.initModality(Modality.WINDOW_MODAL);
			this.configStage.setScene(new Scene(root));
			this.configStage.setResizable(false);
				
			MainConfigViewController controller = loader.getController();
			controller.setStage(this.configStage);
			controller.setModel(this.model);
			controller.setParentController(this);
			controller.init();
			
			this.configStage.show();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@FXML
	/**
	 * �ffnet eine Info-Dialog, welche Informationen �ber die Anwendung enth�lt.
	 */
	private void showInfo() {
		DialogHandler.show(
				AlertType.INFORMATION,
				"Information",
				null,
				"Version: 1.0\n"+
				"Autor: Marcus Meiburg\n\n"+
				"Diese Anwendung wurde im Rahmen einer Bachelorarbeit\n"+
				"an der Fachhochschule L�beck entwickelt.\n\n"+
				"Es handelt sich hierbeit um eine Software zur Simulierung\n"+
				"des Bewegungsmodells unabh�ngiger Agenten. Bei dem\n"+
				"Modell handelt es sich um ein Nullmodell, welches von sowenig"+
				"Parametern wie m�glich beeinflusst wird.");
	}
	
	
	@FXML
	/**
	 * �ffnet das Konfigurationsfenster
	 */
	private void openConfiguration() {
		if(DialogHandler.show(
				AlertType.CONFIRMATION,
				"Achtung!",
				"Die angezeigten Daten werden gel�scht!",
				"Wirklich fortfahren?",
				ButtonType.YES,
				ButtonType.NO)) {
			
			this.reset();
			this.configStage.show();
		}
	}

	
	@FXML
	/**
	 * Schlie�t die Anwendung
	 */
	private void closeSimulator() {
		if(DialogHandler.showCloseWarning()) {
			this.model.stop();
			this.stage.close();
		}
	}
	
	
	/**
	 * Entfernt eine variable Liste von Node Elementen vom Scene-Graphen.
	 * @param nodes - Elemente, welche entfernt werden sollen.
	 */
	private void removeFromSceneGraph(Node ...nodes) {
		this.fxContainerRight.getChildren().removeAll(nodes);
	}
	
	
	/**
	 * F�gt ein noch nicht existierendes Element zum Scene Graphen hinzu.
	 * @param node - Element, welches hinzugef�gt werden soll.
	 */
	private void addToSceneGraph(Node node) {
		if(!this.fxContainerRight.getChildren().contains(node)) {
			this.fxContainerRight.getChildren().add(node);
		}
	}
	

	/**
	 * Startet die Simulation abh�ngig davon ob es eine Visualisierung geben soll.
	 * Daf�r wird der Methode eine boolsche Variable �bergeben die daf�r genutzt wird
	 * zu entscheiden in welchem Modus die Oberfl�che geschaltet wird.
	 * @param visualise 
	 * Bei true, werden auf der Oberfl�che die Bewegungen der Agenten simuliert
	 * Bei false, wird ein Ladebalken angezeigt, welcher angibt, wie weit die Simulation geladen hat.
	 */
	public void start(boolean visualise) {
		this.fxMainView.setDisable(false);
		this.fxMainViewWorldRadius.setText(String.format("%.2f", this.model.getWorld().getRadius()));
		this.fxMaxRepeats.setText(String.format("%d", this.model.getMaxRepeats()));
		this.fxMaxTimesteps.setText(String.format("%d", this.model.getMaxTimesteps()));
		
		this.fxSimulationRate.setDisable(!visualise);
		this.fxMenuPositionData.setDisable(!visualise);
		this.fxMenuInteractionRadius.setDisable(!visualise);
		
		if(visualise) {
			this.fxLeftMainView.setDisable(false);
			this.fxRightMainView.setDisable(false);
			this.addToSceneGraph(this.fxWorldPane);
			if(this.fxMenuInteractionRadius.isSelected()) {
				this.addToSceneGraph(this.fxInteractionCircle);
			}
			
			if(this.fxMenuPositionData.isSelected()) {
				this.addToSceneGraph(this.fxPositionDataPane);
			}
			this.fxMenuPositionData.fire();
			this.initTimer();
			this.processData();
		} else {
			this.fxLeftMainView.setDisable(true);
			this.fxRightMainView.setDisable(false);
			this.fxMenuPositionData.setSelected(false);
			this.fxRepeats.textProperty().unbind();
			this.fxTimesteps.textProperty().unbind();
			this.fxRepeats.setText("...");
			this.fxTimesteps.setText("...");
			this.showMessage("Simulation gestartet");
			this.showProgressBar();
			this.model.start(false);
			this.fxProgressBar.progressProperty().bind(this.model.progressProperty());
		}
	}
	
	
	/**
	 * Beendet die Simulation und schlie�t das Hauptfenster.
	 */
	public void close() {
		this.model.stop();
		this.stage.close();
	}


	/**
	 * Wird am Ende der Simulation aufgerufen und sorgt daf�r das die gesammelten
	 * Daten angezeigt werden und Steuerelemente abgeschaltet werden.
	 */
	private void handleSimulationFinish() {
		this.timer.stop();
		this.removeFromSceneGraph(this.fxWorldPane);
		this.removeFromSceneGraph(this.fxInteractionCircle);
    	this.processData();
		Platform.runLater(new Runnable() {
		    @Override 
		    public void run() {
		    	removeFromSceneGraph(fxWorldPane);
		    	removeFromSceneGraph(fxProgress);
		    	showMessage("Simulation beendet");
		    	fxProgress.setOpacity(0.0);
		    	fxMainView.setDisable(false);
		    	fxLeftMainView.setDisable(false);
		    	fxMenuPositionData.setDisable(false);
		    	fxMenuPositionData.setSelected(true);
		    	fxSimulationRate.setDisable(true);
		    	fxMenuInteractionRadius.setDisable(true);
		    }
		});
	}
	
	
	/**
	 * �bermittelt die Daten des Models und stellt sie auf dem Bildschirm dar.
	 * Damit keine Racecondition zwischen dem Task der Simulation und dem Application Thread
	 * entstehen, werden die darzustellenden Daten via runLater abgefragt.
	 */
	private void processData() {
		// Damit der JavaFX Thread die Daten auswerten kann.
		Platform.runLater(new Runnable() {
		    @Override 
		    public void run() {
		    	// Muss nur einmal instanziiert werden.
		    	if(fxSegments == null) {
		    		fxSegments = Renderer.instantiateSegments(fxPositionDataPane, model.getWorld().getRadius());
		    	}
				fxAgents = Renderer.instatiateWorld(fxWorldPane, model.getWorld());
				fxRepeats.textProperty().bind(Bindings.format("%5d", model.currentRepeatsProperty()));
				fxTimesteps.textProperty().bind(Bindings.format("%5d", model.currentTimestepProperty()));
				fxParametersTableView.setItems(model.getWorld().getAgents());
				fxParametersTableView.getSelectionModel().selectFirst();
				update();
		    }
		});
	}
	
	
	/**
	 * Initialiesiert den Timer.
	 * Die maximalen Aktionen des Timers werden durch die maximalen Zeitschritte multipliziert mit den
	 * maximalen Wiederholungen berechnet.
	 * Diese Aktionen werden in einem bestimmten Intervall ausgel�st und sorgen daf�r, dass sich
	 * das Simulationsmodell aktualiesiert. Und die Beobachter benachrichtigt werden.
	 * Beispiel:
	 * 		10 Zeitschritte, 20 Wiederholungen, Intervall = 1000;
	 * 		Das hei�t, dass der Timer 200 Aktionen ausf�hren kann.
	 * 		Und zwischen den Aktionen jeweils 1sek gewartet wird.
	 */
	private void initTimer() {
		double intervall = Config.TIMER_INTERVALL_IN_MS;
		// Initialiesiert die maximalen Wiederholungen der Timeline
		this.timer.setCycleCount((int)(this.model.getMaxTimesteps() * this.model.getMaxRepeats()));
		// Alle Frames zu begin l�schen
		this.timer.getKeyFrames().clear();
		// F�gt ein Frame hinzu welches nach jedem Intervall das Model aktualisiert.
		this.timer.getKeyFrames().add(new KeyFrame(Duration.millis(intervall), e -> {
			this.model.update();
		}));
		// Timeline starten
		this.timer.play();
	}
	
	
	/**
	 * Aktualiesiert die Elemente auf der Benutzeroberfl�che.
	 */
	private void update() {
		if(this.selectedAgent == null) {
			return;
		}
		
		double maxTimesteps = this.model.getMaxTimesteps();
		double updatefrequence = Math.min(maxTimesteps, Config.SIMULATION_UPDATE_COUNT);
	
		if((this.model.getCurrentTimestep() % (maxTimesteps / Math.max(1, updatefrequence))) == 0) {
			this.showInteractionData();
			this.showPositionData();
		}
	}
	
	
	/**
	 * L�sst die Fortschrittsanzeige auf dem Bildschirm erscheinen. 
	 */
	private void showProgressBar() {
		this.addToSceneGraph(this.fxProgress);
		// Transparenz des Fortschrittsbalkens verringern.
		FadeTransition f = new FadeTransition(Duration.millis(1000), this.fxProgress);
		f.setDelay(Duration.millis(1600));
		f.setFromValue(0.0f);
		f.setToValue(1.0f);
		f.play();
	}

	
	/**
	 * L�sst eine Benachrichtigung auf dem Bildschirm erscheinen.
	 * @param message Nachricht die angezeig werden soll.
	 */
	private void showMessage(String message) {
		this.fxMessage.setOpacity(0.0);
		this.addToSceneGraph(this.fxMessage);
		this.fxMessage.setText(message);
		// Transparenz der Nachricht verringern.
		FadeTransition f = new FadeTransition(Duration.millis(800), this.fxMessage);
		f.setFromValue(0.0f);
		f.setToValue(1.0f);
		f.setCycleCount(2);
		f.setAutoReverse(true);
		f.setOnFinished(e -> {
			this.removeFromSceneGraph(this.fxMessage);
		});
		
		f.play();
	}
	
	
	/**
	 * Stellt die gesammelten Interaktionsdaten als Balkendiagramm dar.
	 */
	private void showInteractionData() {
		this.fxInteractionChart.getData().clear();
		int[] iData;
		long maxRepeats = this.model.getMaxRepeats();
		
		if(this.model.isFinished()) {
			iData = DataHandler.getInstance().getApprInteractionData(this.selectedAgent);
		} else {
			iData = DataHandler.getInstance().getInteractionData(this.selectedAgent);
		}
		
		ObservableList<XYChart.Data<Number, String>> data = FXCollections.observableArrayList();
		for(int i = 0; i < iData.length; i++) {
			// Fallunterscheidung f�r das Abschlie�ende Anzeigen der Daten
			if(this.model.isFinished()) {
				data.add(new XYChart.Data<>(iData[i] / maxRepeats, String.valueOf(i)));
			} else {
				data.add(new XYChart.Data<>(iData[i], String.valueOf(i)));
			}
		}
		this.fxInteractionChart.getData().add(new XYChart.Series<>(data));
	}

	
	/**
	 * F�rbt die Segmente abh�ngig von den Positionsdaten eines Agenten.
	 */
	private void showPositionData() {
		if(this.fxPositionDataPane.isVisible()) {
		    long maxTimesteps = this.model.getMaxTimesteps();
		    long maxRepeats = this.model.getMaxRepeats();
		    int[][] data;
		    if(this.model.isFinished()) {
		    	data = DataHandler.getInstance().getApprPositionData(this.selectedAgent);
		    } else {		    	
		    	data = DataHandler.getInstance().getPositionData(this.selectedAgent);
		    }
			for(int segment = 0; segment < Config.SEGMENT_COUNT; segment++) {
				for(int track = 0; track < Config.TRACK_COUNT; track++) {
					double percent = 0;
					// Fallunterscheidung f�r das Abschlie�ende Anzeigen der Daten
					if(this.model.isFinished()) {
						percent = (((double)data[segment][track] / maxRepeats / maxTimesteps) * 100);
					} else {
						percent = (((double)data[segment][track] / maxTimesteps) * 100);
					}
				    Color color =
				    		percent > 0 ?
				    		this.fxColorImage.getImage().getPixelReader().getColor(Math.min(99, (int) percent), 0) :
				    		null;
					this.fxSegments[segment][track].setFill(color);
				}
			}
		}
	}

	
	/**
	 * Behandelt die Darstellung des selektierten Agenten.
	 */
	private void handleSelectedAgent() {
		Circle circle = this.fxAgents[this.selectedAgent.getId() - 1];
		this.lastAgentColor = circle.getFill();
		circle.setFill(Config.SELECTED_AGENT_FILL);
		circle.setStroke(Config.SELECTED_AGENT_STROKE);
		circle.toFront();	
		
		// Effekt zum hervorheben des Agenten
		ScaleTransition circleScaling = new ScaleTransition(Duration.millis(300), circle);
		circleScaling.setFromX(4f);
		circleScaling.setFromY(4f);
		circleScaling.setToX(1f);
		circleScaling.setToY(1f);
		circleScaling.play();
	}
	
	
	/**
	 * Behandelt den Interaktionsradius des selektierten Agenten.
	 * Verbindet die Koordinaten des selektierten Agenten mit denen des Interaktionsradius.
	 * 
	 * Um zu vermeiden das der Interaktionsradius die Benutzeroberfl�che blockiert, wird ein
	 * "fxInteractionCircle" Element angelegt, welches die Grenzen des Interaktionsradius angibt.
	 * Durch "setClip" wird dann ein Teilst�ck des "fxInteractionCircle" angezeigt.
	 */
	private void handleInteractionRadius() {
		if(this.selectedAgent != null) {
			this.fxInteractionCircle.toFront();
			this.fxWorldPane.toFront();
			
			double paneRadius = this.fxWorldPane.getWidth() / 2;
			double factor = paneRadius / this.model.getWorld().getRadius();
			this.interactionRadius.setRadius(this.selectedAgent.getInteractionRadius() * factor);
			// Bindet die Koordinaten des Interaktionsradius Elements an den selektierten Agenten.
			this.interactionRadius.centerXProperty().bind(this.selectedAgent.getPosition().firstProperty().multiply(factor));
			this.interactionRadius.centerYProperty().bind(this.selectedAgent.getPosition().secondProperty().multiply(factor));
			this.fxInteractionCircle.setClip(this.interactionRadius);
			
			// Effekt zum hervorheben des Interaktionsradius.
			FadeTransition interactionScaling = new FadeTransition(Duration.millis(500), this.fxInteractionCircle);
			interactionScaling.setFromValue(0);
			interactionScaling.setToValue(1f);
			interactionScaling.play();
		}
	}
	
	
	/**
	 * Setzt die Daten des Controllers zur�ck.
	 */
	private void reset() {
		this.model.stop();
		this.timer.stop();
		this.fxMainView.setDisable(true);
		this.selectedAgent = null;
		this.fxProgress.setOpacity(0.0);
		
		this.removeFromSceneGraph(this.fxWorldPane);
		this.removeFromSceneGraph(this.fxPositionDataPane);
		this.removeFromSceneGraph(this.fxInteractionCircle);
		
		this.fxInteractionChart.getData().clear();
		this.fxParametersTableView.getItems().clear();
		this.fxSimulationRate.setValue(Config.START_RATE_OF_SIMULATION);
	}
}
