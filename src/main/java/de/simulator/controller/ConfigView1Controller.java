package de.simulator.controller;

import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Duration;
import de.simulator.controller.handler.DialogHandler;
import de.simulator.controller.handler.ValidationTextField;

/**
 * Steuert die Eingaben, die ein Nutzer im ersten Konfigurationsfester vornehmen kann.
 * Dort k�nnen die Eigenschaften der Simulation und die Grundeigenschaften der Agenten vorgenommen werden.
 * @author Marcus Meiburg
 * @version 1.0
 */
public class ConfigView1Controller extends Controller {

    @FXML private AnchorPane configView1;
    // Konfigurations - Parameter
    @FXML private ValidationTextField fxWorldRadius;
    @FXML private Text fxWorldRadiusError;
    @FXML private ValidationTextField fxNumberOfAgents;
    @FXML private ValidationTextField fxNumberOfTimesteps;
    @FXML private ValidationTextField fxNumberOfRepeats;
    @FXML private ValidationTextField fxNumberOfInitialSteps;
    @FXML private Text fxNumberOfAgentsError;
    @FXML private Text fxNumberOfTimestepsError;
    @FXML private Text fxNumberOfRepeatsError;
    @FXML private Text fxNumberOfInitialStepsError;
    // Agenten - Parameter
    @FXML private ValidationTextField fxPm;
    @FXML private ValidationTextField fxPr;
    @FXML private ValidationTextField fxPd;
    @FXML private ValidationTextField fxInteractionRadius;
    @FXML private ValidationTextField fxStepSize;
    @FXML private Text fxPmError;
    @FXML private Text fxPrError;
    @FXML private Text fxPdError;
    @FXML private Text fxInteractionRadiusError;
    @FXML private Text fxStepSizeError;
    
    
    @Override
    /**
     * Initialisierung des Controllers.
     */
    public void init() {
		// Damit das Fenster sich durch das Anzeigen der Fehlernachricht vergr��ert.
		this.configView1.heightProperty().addListener((obs, oldValue, newValue) -> {
			this.stage.setHeight(newValue.doubleValue() + 105);
		});
		
    	this.initBindings();
    	this.initListener();
    }
    
    
	/**
     * Initialiesiert die Verbindungen zwischen den GUI Elemente und dem Model.
     */
    private void initBindings() {
		this.fxWorldRadiusError.visibleProperty().bind(this.fxWorldRadius.validProperty().not());
		this.fxNumberOfAgentsError.visibleProperty().bind(this.fxNumberOfAgents.validProperty().not());
		this.fxNumberOfTimestepsError.visibleProperty().bind(this.fxNumberOfTimesteps.validProperty().not());
		this.fxNumberOfRepeatsError.visibleProperty().bind(this.fxNumberOfRepeats.validProperty().not());
		this.fxNumberOfInitialStepsError.visibleProperty().bind(this.fxNumberOfInitialSteps.validProperty().not());
		this.fxPmError.visibleProperty().bind(this.fxPm.validProperty().not());
		this.fxPrError.visibleProperty().bind(this.fxPr.validProperty().not());
		this.fxPdError.visibleProperty().bind(this.fxPd.validProperty().not());
		this.fxInteractionRadiusError.visibleProperty().bind(this.fxInteractionRadius.validProperty().not());
		this.fxStepSizeError.visibleProperty().bind(this.fxStepSize.validProperty().not());
	}
    
    
    /**
     * Initialiesiren der Listender, welche auf die Eingaben des Benutzters reagieren.
     */
    private void initListener() {
		this.initErrorListener(this.fxWorldRadiusError, this.fxWorldRadius);
		this.initErrorListener(this.fxNumberOfAgentsError, this.fxNumberOfAgents);
		this.initErrorListener(this.fxNumberOfTimestepsError, this.fxNumberOfTimesteps);
		this.initErrorListener(this.fxNumberOfRepeatsError, this.fxNumberOfRepeats);
		this.initErrorListener(this.fxNumberOfInitialStepsError, this.fxNumberOfInitialSteps);
		this.initErrorListener(this.fxPmError, this.fxPm);
		this.initErrorListener(this.fxPrError, this.fxPr);
		this.initErrorListener(this.fxPdError, this.fxPd);
		this.initErrorListener(this.fxInteractionRadiusError, this.fxInteractionRadius);
		this.initErrorListener(this.fxStepSizeError, this.fxStepSize);
	}


    /**
     * Initialisiert die Listender, welche die Eingabe von Fehlern und dem daraus
     * resultierenden Erscheinen von Fehlernachrichten behandelt.
     * 
     * @param textnode Text, welche die Fehlernachricht enth�lt.
     * @param textField Textfeld, auf dessen Fehler reagiert wird.
     */
	private void initErrorListener(Text textnode, ValidationTextField textField) {
		// Die Fehlernachricht wird mit einer Verz�gerung von 800millisekunden eingeblendet.
		textnode.visibleProperty().addListener(e -> {
	    	FadeTransition f = new FadeTransition(Duration.millis(800), textnode);
	    	f.setFromValue(0);
	    	f.setToValue(1);
	    	f.play();
		});
		
		// Der Text der Nachricht wird auf eine bestimmte Gr��e festgelegt.
		textField.validProperty().addListener((obs, wasValid, isValid) -> {
			if(isValid) {
				textnode.setWrappingWidth(0);
			} else {
				textnode.setWrappingWidth(245);
			}
		});
	}

	
	/**
	 * Pr�ft ob die Textfelder alle g�ltige Werte beinhalten.
	 * @return true, wenn alle Textfelder g�ltig sind, sonst false
	 */
	private boolean isInputValid() {
    	boolean flag = true;
    	return flag & this.fxWorldRadius.isValid() &
    			this.fxNumberOfAgents.isValid() &
    			this.fxNumberOfTimesteps.isValid() &
    			this.fxNumberOfRepeats.isValid() &
    			this.fxNumberOfInitialSteps.isValid() &
    			this.fxPm.isValid() &
    			this.fxPr.isValid() &
    			this.fxPd.isValid() &
    			this.fxInteractionRadius.isValid() &
    			this.fxStepSize.isValid();
    }

	
	/**
	 * �bergibt die g�ltigen Inhalte der Textfelder dem Model.
	 * Wenn die Eingaben falsch sind, wird eine Fehlernachricht angezeigt.
	 * @return true, wenn die Eingaben �bergeben werden konnten, sonst false.
	 */
    public boolean sendDataToModel() {
    	if(this.isInputValid()) {
    		this.model.submitData(
    				this.fxWorldRadius.getValue().doubleValue(),
    				this.fxNumberOfAgents.getValue().intValue(),
    				this.fxNumberOfTimesteps.getValue().intValue(),
    				this.fxNumberOfRepeats.getValue().intValue(),
    				this.fxNumberOfInitialSteps.getValue().intValue(),
    				this.fxPm.getValue().doubleValue(),
    				this.fxPr.getValue().doubleValue(),
    				this.fxPd.getValue().doubleValue(),
    				this.fxInteractionRadius.getValue().doubleValue(),
    				this.fxStepSize.getValue().doubleValue()
    		);
	  		return true;
    	}
		DialogHandler.showInvalidInputError();
		return false;
	}
}
