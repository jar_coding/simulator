package de.simulator.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import de.simulator.controller.handler.DialogHandler;

/**
 * Der MainConfigViewController steuert die Interaktion des Benutzer
 * mit der unteren Buttonleiste der beiden Konfigurationsansichten.
 * Er sorgt daf�r das man zwischen den beiden Ansichten wechseln kann und
 * bietet die M�glichkeit die eingegebenen Daten an das Model zu schicken.
 * @author Marcus Meiburg
 * @version 1.0
 *
 */
public class MainConfigViewController extends Controller {
	
	@FXML private AnchorPane configView1;						// Hauptkomponente des ersten Konfigurationsfensters.
	@FXML private AnchorPane configView2;						// Hauptkomponente des zweiten Konfigurationsfensters.
	
	@FXML private Button fxNavigationButton;					// Navigationsbutton, zum Wechseln zwischen den Fenstern.
	@FXML private CheckBox fxVisualize;							// Checkbox um zu entscheiden ob die Simulation mit oder ohne Visualiesierung gestartet wird.
	
	@FXML private ConfigView1Controller configView1Controller;	// Referenz auf den Controller des ersten Konfigurationsfensters.
	@FXML private ConfigView2Controller configView2Controller;  // Referenz auf den Controller des zweiten Konfigurationsfensters.
	@FXML private VBox fxMainConfigView;						// Hauptkomponente des Konfigurationsfenstercontainers.

	private MainViewController parentController;				// Controller des Hauptfensters.
	
	
	@FXML
    /**
     * Sendet die Daten zum Model, schlie�t das Konfigurationsfenster und startet die Simulation.
     */
    private void startSimulation() {
    	if(this.sendDataToModel()) {
    		this.parentController.start(this.fxVisualize.selectedProperty().not().get());
    		this.stage.close();
    	}
    }

	
    /**
     * Sendet, abh�ngig von der aktuellen Konfigurationsansicht die Daten zum Model,
     * wenn das erfolgreich war, wird true zur�ck gegeben.
     * @return true, wenn die Daten erfolgreich gesendet werden konnten, sonst false.
     */
    private boolean sendDataToModel() {
    	return this.configView1.isVisible() ?
    			this.configView1Controller.sendDataToModel() :
    			this.configView2Controller.sendDataToModel();
    }
    
    
    @FXML
	/**
	 * Wenn der Navigations Button gedr�ckt wird,
	 * wird gepr�ft welche Sicht gerade aktiv ist.
	 * Anhand der Pr�fung wird entschieden, welche Sicht
	 * als n�chstes angezeigt wird.
	 */
    private void navigateBetweenConfiguration() {
    	this.switchViews();
    }
    
    
	/**
	 * Wechselt die Sicht abh�ngig von der aktuellen Sicht
	 */
	private void switchViews() {
		if(this.configView1.isVisible()) {
			/* Sendet hier schon Daten ans Model um in der zweite
			 * Sicht damit arbeiten zu k�nnen */
			if(this.configView1Controller.sendDataToModel()) {
				this.showConfigView2();
				this.configView2Controller.processData();
			}
		} else {
			if(DialogHandler.show(
						AlertType.CONFIRMATION,
						"Achtung!",
						"Wirklich fortfahren?",
						"M�gliche �nderungen gehen verloren!",
						ButtonType.YES,
						ButtonType.NO)) {
					
				this.showConfigView1();
			}
		}
	}
	
	
	/**
	 * Zeigt die zweite Konfigurationssicht an
	 */
	public void showConfigView2() {
		this.fxNavigationButton.setText("< Zur�ck");
		this.configView1.setVisible(false);
		this.configView2.setVisible(true);
	}
	
	
	/**
	 * Zeigt die erste Konfigurationssicht an
	 */
	public void showConfigView1() {
		this.configView1.setVisible(true);
		this.configView2.setVisible(false);
		this.fxNavigationButton.setText("Weiter >");
	}
	
	
	@Override
	/**
	 * Initialiesiert den Controller.
	 */
	public void init() {
		
		// Wenn das Fenster ge�ffnet wird, soll die erste Sicht angezeigt werden.
		this.stage.setOnShowing(e -> {
			this.showConfigView1();
		});
		
		// Wenn das Fenster geschlossen wird, wird auch das Hauptfenster geschlossen.
		this.stage.setOnCloseRequest(e -> {
			if(DialogHandler.showCloseWarning()) {
				this.parentController.close();
			} else {
				e.consume();
			}
		});
		
		this.configView1Controller.setModel(this.model);
		this.configView1Controller.setStage(this.stage);
		this.configView1Controller.init();
		
		this.configView2Controller.setModel(this.model);
		this.configView2Controller.setStage(this.stage);
		this.configView2Controller.init();
	}
	
	
	/**
	 * Setzt den Hauptfenster Controller.
	 * @param controller - Hauptfenster-Controller
	 */
	public void setParentController(MainViewController controller) {
		this.parentController = controller;
	}
	
	
	/**
	 * Gibt den Controller des Hauptfensters zur�ck.
	 * @return Controller des Hauptfensters.
	 */
	public Controller getParentController() {
		return this.parentController;
	}
}
