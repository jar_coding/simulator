package de.simulator.view;

import javafx.scene.CacheHint;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.SVGPath;
import de.simulator.model.Agent;
import de.simulator.model.World;
import de.simulator.model.handler.Config;
import de.simulator.model.handler.math.MathHelpers;

/**
 * Der Renderer f�gt dem SceneGraphen die Visualiesierung der Welt und der Agenten hinzu.
 * Die Agenten werden dabei an die Agentenparameter des Models gebunden um eine 1:1 Aktualiesierung zu gew�hrleisten.
 * @author Marcus Meiburg
 * @version 1.0
 */
public abstract class Renderer {
	/**
	 * Erstellt ein 2Dimensionales Array von SVGPath Objekten.
	 * Die Pfade werden abh�ngig von den in der Config Klasse gespeicherten Werten angelegt.
	 * Die Variable Config.SEGMENT_COUNT gibt die Anzahl der Segmente an und Config.TRACK_COUNT
	 * gibt die Anzahl der Spuren an, welche benutzt werden um die Pfade zu erstellen.
	 * Jedes Segment besitzt die gleiche Fl�che.
	 * 
	 * Die erstellten Pfade, werden zu den Kindelementen der �bergebenen Pane hinzugef�gt.
	 * 
	 * @param pane - Pane auf denen die Pfade angezeigt werden.
	 * @param worldRadius - Radius der Welt.
	 * @return Array, welches alle erstellten Pfade enth�lt.
	 */
	public static SVGPath[][] instantiateSegments(Pane pane, double worldRadius) {
		SVGPath[][] fxSegments = new SVGPath[Config.SEGMENT_COUNT][Config.TRACK_COUNT];
		double radius = pane.getWidth() / 2;
		double factor = radius / worldRadius;
		double trackSize = MathHelpers.calcAreaOfCircularSector(worldRadius) / Config.TRACK_COUNT;

		for(int segmentIndex = 0; segmentIndex < Config.SEGMENT_COUNT; segmentIndex++) {
			for (int trackIndex = 0; trackIndex < Config.TRACK_COUNT; trackIndex++) {
				double oRadius = MathHelpers.calcRadiusOfCircularSector(trackSize * (trackIndex + 1)) * factor;
				double iRadius = MathHelpers.calcRadiusOfCircularSector(trackSize * trackIndex) * factor;
				SVGPath segment = createSegment(
						radius,
						radius,
						oRadius,
						iRadius,
						Config.SEGMENT_SIZE * segmentIndex,
						Config.SEGMENT_SIZE * (segmentIndex + 1),
						Config.WORLD_AREA_COLOR, new Color(0, 0, 0, 0.05)
				);
				fxSegments[segmentIndex][trackIndex] = segment;
				pane.getChildren().add(segment);
			}
		}
		return fxSegments;
	}
	
	
	/**
	 * Erstellt anhand der Parameter ein SVGPath Objekt, in Form eines Kreissegments.
	 * @param centerX - x-Koordinate des Mittelpunkts.
	 * @param centerY - y-Koordinate des Mitelpunkts.
	 * @param oRadius - �u�erer Radius des Kreissegments.
	 * @param iRadius - InnererRadius des Kreissegments.
	 * @param startHeading - Anfangswinkel des Kreissegments.
	 * @param endHeading - Endwinkel des Kreissegments.
	 * @param bgColor - Hintergrundfarbe des Segments.
	 * @param strokeColor - Randfarbe des Segments.
	 * @return SVGPath - Objekt in Form eines Kreissegments.
	 */
	private static SVGPath createSegment(double centerX, double centerY, double oRadius, double iRadius, double startHeading, double endHeading, Color bgColor, Color strokeColor) {
		int segmentCount = Config.SEGMENT_COUNT;
		StringBuilder sb = new StringBuilder();
		// Spezielle Berechnung, falls es nur 1 Segment gibt.
		if(segmentCount == 1) {
			double y1 = centerY + oRadius;
			double y2 = centerY - iRadius;
			sb.append(String.format("M %f %f ", centerX, y1));
			sb.append(String.format("A %f %f 0 1 1 %f %f ", oRadius, oRadius, centerX + 0.01, y1));
			sb.append(String.format("M %f %f ", centerX, y2));
			sb.append(String.format("A %f %f 0 1 0 %f %f", iRadius, iRadius, centerX + 0.01, y2));
		} else {
			double innerX1 = centerX + iRadius * Math.cos(startHeading);
			double innerY1 = centerY + iRadius * Math.sin(startHeading);

			double innerX2 = centerX + iRadius * Math.cos(endHeading);
			double innerY2 = centerY + iRadius * Math.sin(endHeading);

			double outerX1 = centerX + oRadius * Math.cos(startHeading);
			double outerY1 = centerY + oRadius * Math.sin(startHeading);

			double outerX2 = centerX + oRadius * Math.cos(endHeading);
			double outerY2 = centerY + oRadius * Math.sin(endHeading);
			
			sb.append(String.format("M %f %f ", innerX1, innerY1));
			sb.append(String.format("L %f %f ", outerX1, outerY1));
			sb.append(String.format("A %f %f 0 0 1 %f %f ", oRadius, oRadius, outerX2, outerY2));
			sb.append(String.format("L %f %f ", innerX2, innerY2));
			sb.append(String.format("A %f %f 0 0 0 %f %f", iRadius, iRadius, innerX1, innerY1));
		}
		SVGPath path = new SVGPath();
		path.setContent(sb.toString().replaceAll(",", "."));
		path.setFill(bgColor);
		path.setStroke(strokeColor);
		return path;
	}
	
	
	/**
	 * Erstellt die grafische Repr�sentation der Welt und der Agenten.
	 * @param pane - Pane auf welcher die Objekte dargestellt werden sollen.
	 * @param world - Welt, welche dargestellt werden soll.
	 * @return Gibt ein Array mit der Repr�sentation der Agenten zur�ck.
	 */
	public static Circle[] instatiateWorld(Pane pane, World world) {
		Circle[] fxAgents = new Circle[world.getAgents().size()];
		double paneRadius = pane.getWidth() / 2;
		double positionFactor = paneRadius / world.getRadius();
		
		// L�schen der vorhandenen Elemente
		pane.getChildren().clear();
		// Erstellung der Welt
		Circle fxWorld = new Circle(paneRadius, paneRadius, paneRadius, Config.WORLD_AREA_COLOR);
		fxWorld.setStroke(Config.WORLD_BORDER_COLOR);
		pane.getChildren().add(fxWorld);

		// Erstellung der Agenten
		for(Agent agent : world.getAgents()) {
			int size = 4;
			Circle circle = new Circle();
			circle.setFill(new Color(Math.max(0.2, Math.random()), Math.max(0.2, Math.random()), Math.max(0.2, Math.random()), 0.7));
			circle.setStroke(Config.AGENT_STROKE);
			// Die Gr��e eines Agenten w�cht im Verhaltnis der Welt f�r den Betrachter nat�rlich an.
			// Falls die Welt zu Gro� ist, wird eine Mindestgr��e festgelegt
			circle.setRadius(Math.max(size / 2, size * Math.sqrt(positionFactor)));
			circle.centerXProperty().bind(agent.getPosition().firstProperty().multiply(positionFactor));
			circle.centerYProperty().bind(agent.getPosition().secondProperty().multiply(positionFactor));
			// Zur Verbesserung der Rendergeschwindigkeit.
			circle.setCache(true);
			circle.setCacheHint(CacheHint.SPEED);
			
			fxAgents[agent.getId() - 1] = circle;
			pane.getChildren().addAll(circle);
		}
		return fxAgents;
	}
}
