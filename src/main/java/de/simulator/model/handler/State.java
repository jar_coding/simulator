package de.simulator.model.handler;

/**
 * Enum, welches die möglichen Zustände des Bewegungsmodells repräsentiert
 * @author Marcus Meiburg
 * @version 1.0
 */
public enum State {
	MOVE,
	REST
}
