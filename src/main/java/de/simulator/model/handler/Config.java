package de.simulator.model.handler;

import javafx.scene.paint.Color;
/**
 * Die abstrakte Config Klasse enth�lt alle wichtigen einstellbaren Parameter der Simulation.
 * @author Marcus Meiburg
 * @version 1.0;
 */
public abstract class Config {
	
	
	/**
	 * Gibt das Intervall an, in welchem die Simulation aktualiesiert wird.
	 */
	public static final double TIMER_INTERVALL_IN_MS 				= 100;
	
	
	/**
	 * Gibt die Taktrate an, mit welcher das Timerintervall multipliziert wird.
	 */
	public static final double START_RATE_OF_SIMULATION 			= 1;
	
	
	/**
	 * Gibt die Aktualiesierungsrate an, mit welcher die Daten eines selektierten Agenten,
	 * auf der Benutzeroberfl�che aktualiesiert werden.
	 * Beispiel:
	 * SIMULATION_UPDATE_COUNT: 5;
	 * Das hei�t das die Daten 5 mal in einer Wiederholung aktualiesiert werden.
	 * 
	 * Wenn der Wert gr��er als die maximalen Zeitschritte sind, werden die Daten
	 * jeden Zeitschritt aktualiesiert.
	 */
	public static final double SIMULATION_UPDATE_COUNT 				= 100;
	
	
	/**
	 * Gibt die Anzahl der Segmente an, in die die Welt eingeteilt ist.
	 * ACHTUNG: der Wert darf nicht kleiner als 1 sein!
	 */
	public static final int SEGMENT_COUNT 							= 1;
	
	
	/**
	 * Gibt die Anzahl der Spuren an, in die die Welt eingeteilt ist.
	 * ACHTUNG: der Wert darf nicht kleiner als 1 sein!
	 */
	public static final int TRACK_COUNT 							= 3;
	
	
	/**
	 * Gibt an wie gro� ein Segment ist. (Ein Segment wird von zwei Winkeln begrenzt)
	 */
	public static final double SEGMENT_SIZE							= 2 * Math.PI / SEGMENT_COUNT;
	
	
	/**
	 * Gibt an wieviele Agenten sich maximal auf der Welt befinden k�nnen. 
	 */
	public static final int MAX_AGENT_SIZE 							= 100;
	
	
	/**
	 * Gibt die Farbe eines Agenten an. 
	 */
	public static final Color AGENT_COLOR 							= new Color(0,0,0,0.6);
	
	
	/**
	 * Gibt die Farbe des Randes eines Agenten an.
	 */
	public static final Color AGENT_STROKE							= null;
	
	
	/**
	 * Gibt die Farbe eines selektierten Agenten an.
	 */
	public static final Color SELECTED_AGENT_FILL 					= Color.DEEPSKYBLUE;
	
	
	/**
	 * Gibt die Farbe des Randes eines selektierten Agenten an.
	 */
	public static final Color SELECTED_AGENT_STROKE					= null;
	
	
	/**
	 * Gibt die Farbe des Interaktionsrandes eines Agenten an.
	 */
	public static final Color INTERACTION_RADIUS_FILL 				= new Color(1,0,0,0.8);
	
	
	/**
	 * Gibt die Hintergrundfarbe der Welt an.
	 */
	public static final Color WORLD_AREA_COLOR						= new Color(1, 1, 1, 0.2);
	
	
	/**
	 * Gibt die Randfarbe der Welt an.
	 */
	public static final Color WORLD_BORDER_COLOR					= Color.GRAY;
}
