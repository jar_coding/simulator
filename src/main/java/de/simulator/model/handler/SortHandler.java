package de.simulator.model.handler;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

import de.simulator.model.Agent;

/**
 * Der SortHandler ist daf�r zust�ndig die interagierenden Agenten in eine bestimmte Reihenfolge zu bringen.
 * Er bietet die M�glichkeit die Reihenfolge als Array abzufragen.
 * Au�erdem ist er als Singelton implementiert. 
 * @author Marcus Meiburg
 * @version 1.0
 */
public class SortHandler {
	private static SortHandler instance;	// Instanz des SortHandlers.
	private Agent agent;					// Agent dessen Interaktionen gepr�ft werden.
	private Queue<Agent> queue;				// Liste interagierender Agenten.
	
	
	/**
	 * Initialisiert die Queue und erstellt einen Comparator, welcher anhand der Entfernung zum
	 * pr�fenden Agenten sortiert.
	 */
	private SortHandler() {
		this.queue = new PriorityQueue<>(new Comparator<Agent>() {
			@Override
			public int compare(Agent agent1, Agent agent2) {
				if(agent.getPosition().distanceTo(agent1.getPosition()) > agent.getPosition().distanceTo(agent2.getPosition())) {
					return 1;
				} else {
					return -1;
				}
			}	
		});
	}
	
	
	/**
	 * Gibt die Instanz des SortHandlers zur�ck,
	 * falls es noch keine Instanz gibt, wird eine neue erzeugt.
	 * @return Instanz des SortHandlers.
	 */
	public static SortHandler getInstance() {
		if(instance == null) {
			instance = new SortHandler();
		}
		return instance;
	}
	
	
	/**
	 * F�gt einen Agenten zu den interagierenden hinzu
	 * @param agent Agent, welcher hinzugef�gt wird.
	 */
	public void add(Agent agent) {
		this.queue.add(agent);
	}
	
	
	/**
	 * Gibt die sortierte Liste der interagierenden Agenten als Array zur�ck.
	 * @return Liste der Agenten als Array
	 */
	public Agent[] getInteractedAgentsAsArray() {
		return this.queue.toArray(new Agent[this.queue.size()]);
	}
	
	
	/**
	 * �bergibt der Klasse den zu pr�fenden Agenten und setzt die alten Daten zur�ck.
	 * @param agent Agent, welcher gepr�ft werden soll.
	 */
	public void setup(Agent agent) {
		this.queue.clear();
		this.agent = agent;
	}
}
