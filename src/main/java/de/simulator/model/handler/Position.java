package de.simulator.model.handler;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * Die Positionsklasse beschreibt einen Position in einem 2D Raum.
 * @author Marcus Meiburg
 * @version 1.0
 */
public class Position {
	private SimpleDoubleProperty first;	 // erste Koordinate im 2D Raum
	private SimpleDoubleProperty second; // zweite Koordinate im 2D Raum
	
	{
		this.first = new SimpleDoubleProperty();
		this.second = new SimpleDoubleProperty();
	}

	
	/**
	 * Konstruktor, welcher anhand von zwei Koordinaten eine neue Position erstellt.
	 * @param first - erste-Koordinate
	 * @param second - zweite-Koordinate
	 */
	public Position(double first, double second) {
		this.setPosition(first, second);
	}
	
	
	/**
	 * Konstruktor, welcher anhand einer gegebenen Position eine neue Position erstellt.
	 * @param position - gegebene Position
	 */
	public Position(Position position) {
		this.setPosition(position);
	}
	
	
	/**
	 * Setzt die Position auf eine �bergebene Position
	 * @param position - �bergebene Position
	 * @return Gibt die Position zur�ck.
	 */
	public Position setPosition(Position position) {
		this.setPosition(position.getFirst(), position.getSecond());
		return this;
	}
	
	
	/**
	 * Setzt die Position anhand von zwei Koordinaten.
	 * @param first - erste Koordinate.
	 * @param second - zweite Koordinate.
	 * @return Gibt die Position zur�ck.
	 */
	public Position setPosition(double first, double second) {
		this.first.set(first);
		this.second.set(second);
		return this;
	}
	
	
	/**
	 * Gibt die Property der erste Koordinate zur�ck.
	 * @return erste-Koorinate Property
	 */
	public DoubleProperty firstProperty() {
		return this.first;
	}
	
	
	/**
	 * Gibt die Property der zweite Koordinate zur�ck.
	 * @return zweite-Koorinate Property
	 */
	public DoubleProperty secondProperty() {
		return this.second;
	}

	
	/**
	 * Gibt die erste Koordinate zur�ck
	 * @return erste-Koordinate
	 */
	public final double getFirst() {
		return this.first.get();
	}

	
	/**
	 * Gibt die zweite Koordinate zur�ck
	 * @return zweite-Koordinate
	 */
	public final double getSecond() {
		return this.second.get();
	}
	
	
	/**
	 * Gibt die Distanz zwischen zwei Positionen zur�ck
	 * @param position - andere Position
	 * @return Distanz zwischen zwei Positionen
	 */
	public double distanceTo(Position position) {
		return this.distanceTo(position.getFirst(), position.getSecond());
	}
	
	
	/**
	 * Gibt die Distanz zwischen zwei Positionen zur�ck
	 * @param first - erste Koordinate der anderen Position
	 * @param second - zweite Koordinate der anderen Position
	 * @return Distanz zwischen zwei Punkten
	 */
	public double distanceTo(double first, double second) {
		double dx = this.getFirst() - first;
		double dy = this.getSecond() - second;
		return Math.sqrt(dx * dx + dy * dy);
	}
}
