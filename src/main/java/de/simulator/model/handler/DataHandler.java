package de.simulator.model.handler;

import java.util.Arrays;
import de.simulator.model.Agent;
/**
 * Der DataHandler wird ben�tigt um die gesammelten Daten der Simulation zu Speichern
 * und um eine vor formatierte Ausgabe f�r die Visualiesierung zu erstellen.
 * 
 * Der DataHandler ist als Singleton implementiert.
 * 
 * @author Marcus Meiburg
 * @version 1.0
 */
public class DataHandler {

	private static DataHandler instance;		// Referenze auf den DataHandler
	private int[][] interactionData;			// Array, welches pro Agent die Interaktionsdaten bereith�lt
	private int[][][] positionData;				// Array, welche spro Agent die Positionsdaten bereith�lt.
	
	private int[][] apprInteractionData;		// Approximierte Interaktionsdaten der Simulation.
	private int[][][] apprPositionData;			// Approximierte Positionsdaten der Simulation.

	
	/**
	 * Private Konstruktor zum initialiesieren der Writer
	 */
	private DataHandler() {
	}
	
	
	/**
	 * Gibt die Referenz des DataHandler zur�ck,
	 * falls es noch eine Referenz gibt, wird einmalig eine erstellt (Singelton)
	 * @return Referenz des DataHandler
	 */
	public static DataHandler getInstance() {
		if(instance == null) {
			instance = new DataHandler();
		}
		return instance;
	}
	
	
	/**
	 * Initialisierung des DataHandler.
	 * @param numOfAgents Anzahl der Agenten.
	 */
	public void setup(int numOfAgents) {
		this.interactionData = new int[numOfAgents][5];
		this.positionData = new int[numOfAgents][Config.SEGMENT_COUNT][Config.TRACK_COUNT];
		this.apprInteractionData = new int[numOfAgents][5];
		this.apprPositionData = new int[numOfAgents][Config.SEGMENT_COUNT][Config.TRACK_COUNT];
	}
	
	
	/**
	 * Setzt die Daten f�r die Darstellung auf dem Bildschirm zur�ck.
	 */
	public void reset() {
		for(int i = 0; i < this.interactionData.length; i++) {
			Arrays.fill(this.interactionData[i], 0);
			this.positionData[i] = new int[Config.SEGMENT_COUNT][Config.TRACK_COUNT];
		}
	}
	
	
	/**
	 * Erstellung der Positionsdaten f�r die Visualiesierung.
	 * @param agent Agent, dessen Positionsdaten erstellt werden.
	 */
	public void createPositionData(Agent agent) {
		int agentIndex = agent.getId() - 1;
		int segment = (int)agent.getSectorPosition().getFirst();
		int track = (int)agent.getSectorPosition().getSecond();
		this.positionData[agentIndex][segment][track]++;
		this.apprPositionData[agentIndex][segment][track]++;
	}

	
	/**
	 * Erstellung der Interaktionsdaten f�r die Visualiesierung.
	 * @param agent Agent, dessen Interaktionsdaten erstellt werden.
	 */
	public void createInteractionData(Agent agent) {
		int agentIndex = agent.getId() - 1;
		int count = agent.getInteractedAgents().length;
		// Vergr��ert das Array der Interaktionen falls notwendig.
		int[] tmp = interactionData[agentIndex];
		if(count >= tmp.length) {
			this.interactionData[agentIndex] = Arrays.copyOf(tmp, count + 1);
		}
		int[] appTmp = this.apprInteractionData[agentIndex];
		if(count >= appTmp.length) {
			this.apprInteractionData[agentIndex] = Arrays.copyOf(appTmp, count + 1);
		}
		this.interactionData[agentIndex][count]++;
		this.apprInteractionData[agentIndex][count]++;
	}
	
	
	/**
	 * Gibt die Interaktionsdaten des �bergebenen Agenten zur�ck.
	 * @param agent - Agent dessen Interaktionsdaten zur�ck gegeben werden sollen
	 * @return Interaktionsdaten des Agenten
	 */
	public int[] getInteractionData(Agent agent) {
		return this.interactionData[agent.getId() - 1];
	}
	
	
	/**
	 * Gibt die Positionsdaten des �bergebenen Agenten zur�ck.
	 * @param agent - Agent dessen Positionsdaten zur�ck gegeben werden sollen
	 * @return Positionsdaten des Agenten
	 */
	public int[][] getPositionData(Agent agent) {
		return this.positionData[agent.getId() - 1];
	}
	
	
	/**
	 * Gibt die approximierten Positionsdaten des �bergebenen Agenten zur�ck.
	 * @param agent - Agent dessen Positionsdaten zur�ck gegeben werden sollen
	 * @return Positionsdaten des Agenten
	 */
	public int[][] getApprPositionData(Agent agent) {
		return this.apprPositionData[agent.getId() - 1];
	}
	
	
	/**
	 * Gibt die approximierten Interaktionsdaten des �bergebenen Agenten zur�ck.
	 * @param agent - Agent dessen Interaktionsdaten zur�ck gegeben werden sollen
	 * @return Interaktionsdaten des Agenten
	 */
	public int[] getApprInteractionData(Agent agent) {
		return this.apprInteractionData[agent.getId() - 1];
	}
}
