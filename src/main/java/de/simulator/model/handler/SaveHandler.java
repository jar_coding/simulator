package de.simulator.model.handler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.simulator.model.Agent;

/**
 * Der SaveHandler ist als Singelton implementiert und ist f�r das Speichern
 * der enstandenen Simulationsdaten in eine .csv Datei zust�ndig.
 * Dabei werden die Interaktions- und Positionsdaten seperat gespeichert.
 * 
 * Die Datein werden im Ordner "output" abgelegt und sind dort in Ordnern
 * nach Tagen sortiert.
 * @author Marcus Meiburg
 * @version 1.0
 */
public class SaveHandler {
	private static SaveHandler instance;		// Instanz des SaveHandlers
	private BufferedWriter interactionWriter;	// Writer um die Interaktionsdaten zu speichern.
	private BufferedWriter positionWriter;		// Writer um die Positionsdaten zu speichern.
	private ArrayList<String> interactionBuffer;// Buffert die Interaktionsdaten pro Wiederholung.
	private ArrayList<String> positionBuffer;	// Buffert die Positionsdaten pro Wiederholung.
	
	private int numOfAgents;					// Anzahl der Agenten der Simulation
	
	private String interactionFilePath;
	private String positionFilePath;
	
	// Initialisierung der privaten Attribute.
	{
		this.interactionBuffer =  new ArrayList<>(10000000);
		this.positionBuffer = new ArrayList<>(10000000);
	}
	
	/**
	 * Privater Konstruktor
	 */
	private SaveHandler() {}
	
	
	/**
	 * Gibt die Referenz auf den SaveHandler zur�ck.
	 * Falls es noch keine Instanz gibt, wird eine neue angelegt.
	 * @return Referenz auf den SaveHandler;
	 */
	public static SaveHandler getInstance() {
		if(instance == null) {
			instance = new SaveHandler();
		}
		return instance;
	}

	
	/**
	 * Bereitet den SaveHandler auf das Speichern der Daten vor.
	 * 
	 * Das Format der Datei sieht dabei wie folgt aus:
	 * 
	 * 	NAME_DER_DATEN [a(ANZAHL_DER_AGENTEN)_w(GR��E_DER_WELT)]_(STUNDE-MINUTE-SEKUNDE).csv
	 * 
	 * @param numOfAgents - Anzahl der Agenten der Simulation.
	 * @param worldRadius - Gr��e der Welt der Simulation.
	 */
	public void setup(int numOfAgents, double worldRadius) {
		this.numOfAgents = numOfAgents;
		SimpleDateFormat dirFormat = new SimpleDateFormat( "yyyy-MM-dd" );
		SimpleDateFormat fileFormat = new SimpleDateFormat( "HH-mm-ss" );
		Date date = new Date();
		String timestamp = dirFormat.format(date);
		String time = fileFormat.format(date);

		File dir = new File("output\\" + timestamp);
		dir.mkdirs();
		String pathToDir = dir.getAbsolutePath();
	
		try {
			this.interactionFilePath = String.format("%s\\interactionData[a%d_w%.2f]_%s.csv", pathToDir, numOfAgents, worldRadius,time);
			this.interactionWriter = new BufferedWriter(new FileWriter(this.interactionFilePath, true), 10000000);
		} catch (IOException e) {
			System.err.println(String.format("Datei im Pfad: \"%s\" konnte nicht gefunden werden!", this.interactionFilePath));
			System.exit(-1);
		}
		
		try {
			this.positionFilePath = String.format("%s\\positionData[a%d_w%.2f]_%s.csv", pathToDir, numOfAgents, worldRadius,time);
			this.positionWriter = new BufferedWriter(new FileWriter(this.positionFilePath, true), 10000000);
		} catch (IOException e) {
			System.err.println(String.format("Datei im Pfad: \"%s\" konnte nicht gefunden werden!", this.positionFilePath));
			System.exit(-1);
		}
		
		// Erstellung des Headers der Ausgabe
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("Wiederholung;Zeitschritt;"));
		for(int i = 0; i < numOfAgents; i++) {
			sb.append(String.format("Agent %d;", i + 1));
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("\n");
		
		this.interactionBuffer.add(sb.toString());
		this.positionBuffer.add(sb.toString());
	}
	
	
	/**
	 * Speichert die aktuell gesammelten Daten auf der Festplatte
	 */
	public void saveToFile() {
		// Speichert die Daten pro Wiederholung ab um den Buffer nicht zu �berlasten.
		try {
			for(String s : this.interactionBuffer) {
				this.interactionWriter.write(s);
			}
			this.interactionWriter.flush();
			this.interactionBuffer.clear();
			
			for(String s : this.positionBuffer) {
				this.positionWriter.write(s);
			}
			this.positionWriter.flush();
			this.positionBuffer.clear();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Speichert die Interaktions- und Positionsdaten des �bergeben Agenten
	 * in einem Buffer ab.
	 * @param agent - Agent dessen Daten gespeichert werden sollen.
	 * @param repeat - aktuelle Wiederholung der Simulation.
	 * @param timestep - aktueller Zeitschritt der Simulation.
	 */
	public void save(Agent agent, long repeat, long timestep) {
		StringBuilder sb = new StringBuilder();
		
		if(agent.getId() == 1) {
			sb.append(String.format("%d;%d;", repeat, timestep));
			this.interactionBuffer.add(sb.toString());
			this.positionBuffer.add(sb.toString());
		}
		this.saveInteractionData(agent);
		this.savePositionnData(agent);
	}
	
	
	/**
	 * Speichert die Positionsdaten eines Agenten.
	 * @param agent Agent dessen Daten gespeichert werden.
	 */
	private void savePositionnData(Agent agent) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("[%.0f,%.0f];", agent.getSectorPosition().getFirst(), agent.getSectorPosition().getSecond()));

		if(agent.getId() == this.numOfAgents) {
			sb.deleteCharAt(sb.length() - 1);
			sb.append("\n");
		}
		this.positionBuffer.add(sb.toString());
	}

	
	/**
	 * Speichert die Interaktionsdaten eines Agenten.
	 * @param agent Agent dessen Daten gespeichert werden.
	 */
	private void saveInteractionData(Agent agent) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if(agent.isAlone()) {
			sb.append("0");
		} else {
			for(Agent a : agent.getInteractedAgents()) {
				sb.append(String.format("%d,", a.getId()));
			}
			sb.deleteCharAt(sb.length() - 1);
		}
		sb.append("];");
		
		if(agent.getId() == this.numOfAgents) {
			sb.deleteCharAt(sb.length() - 1);
			sb.append("\n");
		}
		this.interactionBuffer.add(sb.toString());
	}
	
	
	/**
	 * L�scht die derzeitig angelegten Dateien.
	 */
	public void delete() {
		
		if(this.interactionWriter == null || this.positionWriter == null) {
			return;
		}
		
		try {
			this.interactionWriter.close();
			this.positionWriter.close();
		} catch (IOException e) {
			System.out.println("Datei konnte nicht gefunden werden!");
			System.exit(-1);
		}
		this.deleteFile(this.interactionFilePath);
		this.deleteFile(this.positionFilePath);
	}
	
	
	/**
	 * L�scht die Datei im angegebenen Pfad.
	 * @param path Pfad der Datei die gel�scht werden soll.
	 */
	private void deleteFile(String path) {
		File file = new File(path);
		file.delete();
	}
	
	
	/**
	 * Schlie�t die Writer
	 */
	public void close() {
		try {
			this.interactionWriter.close();
			this.positionWriter.close();
		} catch (IOException e) {
			// TODO: Alert
			System.out.println("Datei konnte nicht gefunden werden!");
			System.exit(-1);
		}
	}
}
