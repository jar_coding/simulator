package de.simulator.model.handler.math;

import de.simulator.model.handler.Config;

/**
 * Die MathHelpers Klasse bietet einige Methoden zur Vereinfachung von mathematischen Problemen.
 * @author Marcus Meiburg
 * @version 1.0
 */
public class MathHelpers {
	
	public static final MersenneTwisterFast random;	// Zufallszahlengenerator
	private static final VonMises vonMises;			// VonMises Verteilung
	
	// Statischer Initialiesierungsblock
	static {
		random = new MersenneTwisterFast();
		vonMises = new VonMises(1.0, random);
	}
	
	
	/**
	 * Gibt einen Wert im Wertebereich von [min,max] zurück
	 * @param min - Minimalwert.
	 * @param max - Maximalwert.
	 * @return Zufälliger Wert zwischen min und max.
	 */
	public static double nextDoubleInRange(double min, double max) {
		return (random.nextDouble() * (max - min)) + min;
	}

	
	/**
	 * Gibt einen normalverteilen Wert zwischen -PI und PI zurück.
	 * @return normalverteileter Wert.
	 */
	public static double vonMisesDistribution() {
		return vonMises.nextDouble();
	}
	
	
	/**
	 * Gibt den Flächeninhalt eines Kreissektors zurück.
	 * @param radius - Radius zur Berechnung des Flächeninhalts.
	 * @return Flächeninhalt des Kreissektors.
	 */
	public static double calcAreaOfCircularSector(double radius) {
		return (Config.SEGMENT_SIZE * radius * radius) / 2;
	}
	
	
	/**
	 * Gibt den Radius eines Kreissektors zurück
	 * @param area - Flächeninhalt zur Berechnung des Radius.
	 * @return Radius des Kreissektors.
	 */
	public static double calcRadiusOfCircularSector(double area) {
		return Math.sqrt(area * 2 / Config.SEGMENT_SIZE);
	}
}
