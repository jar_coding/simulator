package de.simulator.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import de.simulator.model.handler.Position;
import de.simulator.model.handler.math.MathHelpers;

/**
 * Die Welt beschreibt den Handlungsspielraum der Agenten.
 * Dabei wird sie durch einen Kreis mit einem bestimmten Radius dargestellt.
 * Der Radius gibt die Gr��e der Welt und damit die Grenzen des Handlungsspielraumes an.
 * 
 * @author Marcus Meiburg
 * @version 1.0
 */
public class World {
	
	public static Position center;			// Mittelpunkt der Welt
	private ObservableList<Agent> agents;	// Agenten die sich auf der Welt befinden.
	private double radius;					// Gr��e der Welt (Radius des Kreises).

	// Initialisierung der privaten Attribute.
	{
		this.agents = FXCollections.observableArrayList();
	}
	
	
	public World(double radius) {
		this.radius = radius;
		center = new Position(radius, radius);
	}

	
	/**
	 * Gibt eine beobachtbare Liste der Agenten der Welt zur�ck
	 * @return Liste der Agenten
	 */
	public ObservableList<Agent> getAgents() {
		return this.agents;
	}

	
	/**
	 * Gibt den Radius der Welt zur�ck
	 * @return Radius der Welt
	 */
	public double getRadius() {
		return this.radius;
	}
	
	/**
	 * Aktualiesiert die Welt
	 * Pr�ft den Interaktionsradius der Agenten und pr�ft
	 * in welchem Sektor der Welt sie sich aufhalten.
	 * Zus�tzlich wird das Bewegungsmodell eines jeden Agenten
	 * durchlaufen.
	 */
	public void update() {
		// Durchlaufen des Bewegungsmodells
		for(Agent agent : this.agents) {
			agent.update();
		}
	}
	
	
	/**
	 * Durchl�uft die initialen Zeitschritte der Simulation
	 */
	public void goThroughInitialSteps() {
		for(Agent agent : this.agents) {
			agent.goThroughInitialSteps();
		}
	}
	
	
	/**
	 * Berechnet f�r jeden Agenten eine neue Position und setzt das Bewegungsmodell zur�ck.
	 */
	public void reset() {
		for(Agent agent : this.agents) {
			this.randomizePositionOf(agent);
			agent.reset();
		}
	}

	
	/**
	 * Erstellt Agenten basierend auf den �bergebenen Parametern.
	 * @param numberOfAgents Anzahl der zu erstellenden Agenten
	 * @param pm Wahrscheinlichkeit eines Agenten sich zu bewegen
	 * @param pr Wahrscheinlichkeit eines Agenten zu verweilen
	 * @param pt Wahrscheinlichkeit eines Agenten die Richtung zu �ndern.
	 * @param stepsize Schrittweite eines Agenten
	 * @param interactionRadius Interaktionsradius eines Agenten.
	 */
	public void createAgents(int numberOfAgents, double pm, double pr, double pt, double stepsize, double interactionRadius) {
		this.agents.clear();
		for(int i = 0; i < numberOfAgents; i++) {
			int id = this.agents.size() + 1;
			Agent agent = new Agent(id, pm, pr, pt, stepsize, interactionRadius);
			this.randomizePositionOf(agent);
			this.agents.add(agent);
		}
	}
	
	
	/**
	 * Generiert eine zuf�llig Position auf der Welt und �bergibt diese einem Agenten.
	 * @param agent - Agent, dessen Position gesetzt werden soll.
	 */
	private void randomizePositionOf(Agent agent) {
		agent.setHeading(MathHelpers.nextDoubleInRange(0, 2 * Math.PI));
		double heading = agent.getHeading();
		double r = (this.radius * Math.sqrt(MathHelpers.random.nextDouble()));
		double x = this.radius + r * Math.cos(heading);
		double y = this.radius + r * Math.sin(heading);
		agent.setPosition(x,y);
	}
}
