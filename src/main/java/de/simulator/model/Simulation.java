package de.simulator.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.concurrent.Task;
import de.simulator.model.handler.Config;
import de.simulator.model.handler.DataHandler;
import de.simulator.model.handler.Position;
import de.simulator.model.handler.SaveHandler;
import de.simulator.model.handler.SortHandler;
import de.simulator.model.handler.math.MathHelpers;

/**
 * Die Simulationsklasse enth�lt alle Metadaten, die ben�tigt werden um eine Simulations durchzuf�hren.
 * @author Marcus Meiburg
 * @version 1.0
 */
public class Simulation {
	
	private LongProperty currentTimestep;	// gibt den aktuellen Zeitschritt an
	private LongProperty currentRepeat;		// gibt die aktuelle Wiederholung an
	private LongProperty maxTimesteps;		// gibt die maximalen Zeitschritte an
	private LongProperty maxRepeats;		// gibt die maximalen Wiederholungen an
	private World world;					// gibt den Lebensraum der Agenten an
	private BooleanProperty finished;		// gibt an ob die Simulation beendet wurde.
	private double initSteps;				// gibt die Anzahl der Initialschritte an.

	private Task<Void> updateTask;			// Task welcher in einem eigenen Thread im Nicht-Visualisierungsmodus abgearbeitet wird.

	// Initialiesiert die privaten Attribute
	{
		this.currentTimestep 	= new SimpleLongProperty(1);
		this.currentRepeat 		= new SimpleLongProperty(1);
		this.maxRepeats 		= new SimpleLongProperty();
		this.maxTimesteps 		= new SimpleLongProperty();
		this.finished 			= new SimpleBooleanProperty(false);
	}

	
	/**
	 * Erstellt die Welt und erzeugt die Agenten anhand
	 * der �bergebenen Parameter. Zus�tzlich werden die
	 * maximalen Zeitschritte und Wiederholungen festgelegt.
	 * 
	 * @param worldRadius - Radius der Welt.
	 * @param numOfAgents - Anzahl der Agenten.
	 * @param maxTimesteps - maximale Anzahl an Zeitschritten.
	 * @param maxRepeats - maximale Anzahl an Wiederholungen.
	 * @param numOfInitTimesteps - Anzahl der Initialen Zeitschritte.
	 * @param pm - Bewegungswahrscheinlichkeit aller Agenten.
	 * @param pr - Wahrscheinlichkeit zu Verweilen.
	 * @param pt - Wahrscheinlichkeit einer Richtungs�nderung.
	 * @param interactionRadius - Interaktionsradius eines Agenten.
	 * @param stepsize - L�nge eines Schrittes.
	 */
	public void submitData(
			double worldRadius,
			int numOfAgents,
			int maxTimesteps,
			int maxRepeats,
			int numOfInitTimesteps,
			double pm,
			double pr,
			double pt,
			double interactionRadius,
			double stepsize) {

		DataHandler.getInstance().setup(numOfAgents);
	    SaveHandler.getInstance().setup(numOfAgents, worldRadius);

		this.maxTimesteps.set(maxTimesteps);
		this.maxRepeats.set(maxRepeats);
		this.world = new World(worldRadius);
		this.world.createAgents(numOfAgents, pm, pr, pt, stepsize, interactionRadius);
		this.initSteps = numOfInitTimesteps;
	}
	
	
	/**
	 * Durchl�uft die initialen Zeitschritte der Simulation um
	 * f�r jeden Agenten einen zuf�lligen Startpunkt zu erhalten.
	 */
	private void goThroughInitialSteps() {
		for(double i = 0; i < this.initSteps; i++) {
			this.world.goThroughInitialSteps();
		}
	}
	
	
	/**
	 * Bereitet die Simulation auf den Start vor und pr�ft ob
	 * eine Visualiesierung erforderlich ist oder nicht.
	 * @param startFromConsole - Pr�ft ob die Simulation von der Console gestartet wurde.
	 */
	public void start(boolean startFromConsole) {
		this.currentRepeat.set(1);
		this.currentTimestep.set(1);
		this.finished.set(false);

		System.out.println("Simulation wurde gestartet");
		
		if(startFromConsole) {
			this.startFromConsole();
		} else {
			this.startFromGUI();
		}
	}
	
	
	/**
	 * Stopt die Simulation.
	 */
	public void stop() {
		if(this.updateTask != null) {
			this.updateTask.cancel();
		}
		/* 
		 * Um Datenm�ll zu vermeiden, werden die Dateien
		 * mit den gespeicherten Daten gel�scht, falls die Simulation
		 * noch nicht beendet ist.
		 */
		if(!this.isFinished()) {
			SaveHandler.getInstance().delete();
		}
		
		this.currentRepeat.set(1);
		this.currentTimestep.set(1);
		this.finished.set(false);
	}	
	
	/**
	 * Startet die Simulation ohne Visualisierung.
	 */
	private void startFromConsole() {
		for(double repeat = this.getCurrentRepeat(); repeat <= this.getMaxRepeats(); repeat++) {
			for(double timestep = this.getCurrentTimestep(); timestep <= this.getMaxTimesteps(); timestep++) {
				this.update();
			}
		}
		// Nach der Simulation Anwendung beenden.
		System.exit(1);
	}
	
	
	/**
	 * Durchl�uft die Simulation ohne einen Timer zu verwenden.
	 * Die Laufzeitkomplexit�t ist abh�ngig von den maximalen Wiederholungen, 
	 * den maximalen Zeitschritten der Simulation, der Anzahl der Agenten,
	 * vom Bewegungsmodell dieser und den Daten welche die Agenten sammeln.
	 */
	private void startFromGUI() {
		final long maxRepeats = this.getMaxRepeats();
		final long maxTimesteps = this.getMaxTimesteps();

		this.updateTask = new Task<Void>() {
		    @Override public Void call() {
				int count = 1;
				for(long repeat = getCurrentRepeat(); repeat <= maxRepeats && !this.isCancelled(); repeat++) {
					for(long timestep = getCurrentTimestep(); timestep <= maxTimesteps && !this.isCancelled(); timestep++) {
						this.updateProgress(count, maxRepeats * maxTimesteps);
						update();
						count++;
					}
				}
		        return null;
		    }
		};
		new Thread(this.updateTask).start();
	}
    
	
	/**
	 * Aktualiesiert das Simulationsmodell und z�hlt die Zeitschritte hoch
	 */
	public void update() {
		// Erzeugt Rohdaten.
		for(Agent agent : this.world.getAgents()) {
			this.analyseInteraction(agent);
			this.analysePosition(agent);
			DataHandler.getInstance().createInteractionData(agent);
			DataHandler.getInstance().createPositionData(agent);		
			SaveHandler.getInstance().save(agent, this.getCurrentRepeat(), this.getCurrentTimestep());
		}
		this.world.update();
		this.incCurrentTimestep();
	}
	
	
	/**
	 * Pr�ft den Interaktionsradius eines Agenten und gibt die darin befindlichen Agenten,
	 * geordnet nach der Distanz zum �berpr�ften Agenten, als Array an den zu �berpr�fenden Agenten.
	 * @param agent Agent, welcher analysiert wird.
	 */
	private void analyseInteraction(Agent agent) {
		SortHandler sort = SortHandler.getInstance();
		sort.setup(agent);
		for(Agent other : this.world.getAgents()) {
			if(agent.interactWith(other)) {
				sort.add(other);
			}
		}
		agent.setInteractions(sort.getInteractedAgentsAsArray());
	}
	
	
	/**
	 * Pr�ft in welchem Bereich der Welt ein Agent liegt.
	 * Dabei wird die Welt in Segmente und Spuren eingeteilt.
	 * Dem Agenten wird der Index des Segments und Spur als Position �bergeben.
	 * @param agent Agent, welcher Analysiert wird.
	 */
	private void analysePosition(Agent agent) {
		Position p = agent.getPosition();
		double radius = this.world.getRadius();
		double dx = p.getFirst() - radius;
		double dy = p.getSecond() - radius;
		double heading = Math.atan2(dy, dx);
		heading += heading < 0 ? 2 * Math.PI : 0;
		double trackAreaSize = MathHelpers.calcAreaOfCircularSector(radius) / Config.TRACK_COUNT;
		
		int segment = (int)(heading / Config.SEGMENT_SIZE);
		int track = (int)(MathHelpers.calcAreaOfCircularSector(p.distanceTo(World.center)) / trackAreaSize);
		agent.setSectorPosition(segment, track);
	}
	
	/**
	 * Gibt zur�ck ob die Simulation beendet worden ist.
	 * @return true - wenn die Simulation beendet worden ist.
	 */
	public final boolean isFinished() {
		return this.finished.get();
	}
	
	
	/**
	 * Read-Only Variable die angibt ob die Simulations bereits beendet wurde.
	 * @return Simulation wurde beendet.
	 */
	public ReadOnlyBooleanProperty finishedProperty() {
		return this.finished;
	}

	/**
	 * Gibt die Verbindung zum Fortschritt der Simulation zur�ck.
	 * @return Verbindung zum Fortschritt der Simulation.
	 */
	public ReadOnlyDoubleProperty progressProperty() {
		return this.updateTask.progressProperty();
	}
	
	
	/**
	 * Gibt die Welt der Simulation zur�ck
	 * @return Welt der Simulation
	 */
	public final World getWorld() {
		return this.world;
	}
	
	
	/**
	 * Read-Only Variable der maximalen Zeitschritte.
	 * @return maximale Zeitschritte.
	 */
	public ReadOnlyLongProperty maxTimestepsProperty() {
		return this.maxTimesteps;
	}
	
	
	/**
	 * Gibt die maximalen Zeitschritte zur�ck.
	 * @return maximale Zeitschritte.
	 */
	public final long getMaxTimesteps() {
		return this.maxTimesteps.get();
	}
	
	
	/**
	 * Read-Only Variable der maximalen Wiederholungen.
	 * @return maximale Wiederholungen.
	 */
	public ReadOnlyLongProperty maxRepeatsProperty() {
		return this.maxRepeats;
	}
	
	
	/**
	 * Gibt die maximalen Wiederholungen zur�ck.
	 * @return maximalen Wiederholungen.
	 */
	public final long getMaxRepeats() {
		return this.maxRepeats.get();
	}
	
	
	/**
	 * Read-Only Variable des aktuellen Zeitschritts.
	 * @return aktueller Zeitschritt.
	 */
	public ReadOnlyLongProperty currentTimestepProperty() {
		return this.currentTimestep;
	}
	
	
	/**
	 * Gibt den aktuellen Zeitschritt zur�ck.
	 * @return aktueller Zeitschritt.
	 */
	public final long getCurrentTimestep() {
		return this.currentTimestep.get();
	}
	
	
	/**
	 * Gibt die aktuelle Wiederholung zur�ck.
	 * @return aktuelle Wiederholung. 
	 */
	public final long getCurrentRepeat() {
		return this.currentRepeat.get();
	}
	
	
	/**
	 * Ready-Only Variable der aktuellen Wiederholung.
	 * @return aktuelle Wiederholung.
	 */
	public ReadOnlyLongProperty currentRepeatsProperty() {
		return this.currentRepeat;
	}
	
	
	/**
	 * Erh�ht die aktuellen Zeitschritte, falls dieser noch nicht das Maximum erreicht haben.
	 * Andernfalls wird der Z�hler der aktuellen Wiederholungen erh�ht.
	 */
	private void incCurrentTimestep() {
		if(this.getCurrentTimestep() < this.getMaxTimesteps()) {
			this.currentTimestep.set(this.getCurrentTimestep() + 1);
		} else {
			this.incRepeats();
		}
	}
	
	
	/**
	 * Erh�ht die Wiederholungen, fall die maximalen Wiederholungen noch nicht
	 * erreicht worden sind und setzt den aktuellen Zeitschritt auf eins zur�ck.
	 * 
	 * Zus�tzlich werden die aktuell gesammelten Daten in einer Datei gespeichert.
	 */
	private void incRepeats() {
		SaveHandler.getInstance().saveToFile();
		if(this.getCurrentRepeat() < this.getMaxRepeats()) {
			this.world.reset();
			this.currentRepeat.set(this.getCurrentRepeat() + 1);
			this.currentTimestep.set(1);
			this.goThroughInitialSteps();
			DataHandler.getInstance().reset();
		} else {
			this.repeatsFinished();
		}
	}
	
	
	/**
	 * Wird aufgerufen wenn alle Wiederholungen beendet worden sind.
	 */
	private void repeatsFinished() {
		this.finished.set(true);
		System.out.println("Simulation wurde beendet");
		SaveHandler.getInstance().close();
	}
}
