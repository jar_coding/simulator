package de.simulator.model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import de.simulator.model.handler.Position;
import de.simulator.model.handler.State;
import de.simulator.model.handler.math.MathHelpers;
/**
 * Das Verhalten eines Agenten wird durch wenige Parameter gesteuert.
 * Durch den Aufruf der Update-Methode wird ein Zustandautomat durchlaufen.
 * Der Automat steuert das Bewegungsmuster des Agenten, das hei�t bei jedem
 * Aufruf wird die Position des Agenten auf der Welt neu bestimmt.
 * @author Marcus Meiburg
 * @version 1.0
 */
public class Agent {
	
	private Map<State, Map<State, Integer>> stateMap;	// Markovkette 1. Ordnung
	private int changesOfTurnsCounter;					// Z�hler f�r die Anzahl der Richtungs�nderungen
	private int updatesCounter;							// Z�hler der ausgef�hrten Update Aufrufe
	private State curState;								// Derzeitiger Bewegungszustand
	private State preState;								// Letzter Bewegungszustand
	private Agent[] interactedAgents;					// Derzeitige Agenten im Interaktionsradius
	private Position sectorPosition;					// Koordinaten des Sektors
	
	private int id;										// Eindeutige Identifikationsnummer
	private Position position;							// Position des Agenten
	private double heading;								// Richtung des Agenten
	
	private final DoubleProperty pm;					// Bewegungswahrscheinlichkeit
	private final DoubleProperty pr;					// Verweilwahrscheinlichkeit
	private final DoubleProperty pt;					// Richtungs�nderungswahrscheinlichkeit
	
	private final DoubleProperty curPm;					// Gesch�tzte Bewegungswahrs.
	private final DoubleProperty curPr;					// Gesch�tzte Verweilwahrs.
	private final DoubleProperty curPt;					// Gesch�tzte Wahrs. die Richtung zu �ndern.
	
	private final DoubleProperty stepSize;				// Schrittweite
	private final DoubleProperty interactionRadius;		// Interaktionsradius

	// Initialisierung der privaten Klassenattribute.
	{
		this.stateMap			= new LinkedHashMap<>();
		this.position 			= new Position(0, 0);
		this.heading 			= 0.0;
		this.pm 				= new SimpleDoubleProperty();
		this.pr 				= new SimpleDoubleProperty();
		this.pt 				= new SimpleDoubleProperty();

		this.curPm 				= new SimpleDoubleProperty();
		this.curPr 				= new SimpleDoubleProperty();
		this.curPt 				= new SimpleDoubleProperty();
		
		this.stepSize 			= new SimpleDoubleProperty();
		this.interactionRadius 	= new SimpleDoubleProperty();
		this.curState			= State.MOVE;
		this.preState			= State.MOVE;
		this.sectorPosition  	= new Position(0, 0);
	}

	
	/**
	 * Konstruktor
	 * @param id - ID des Agenten
	 * @param pm - Bewegungswahrscheinlichkeit
	 * @param pr - Verweilwahrscheinlichkeit
	 * @param pt - Wahscheinlichkeit die Richtung zu �ndern.
	 * @param stepsize - Schrittweite des Agenten
	 * @param interactionRadius - Interaktionsradius des Agenten
	 */
	public Agent(int id, double pm, double pr, double pt, double stepsize, double interactionRadius) {
		this.id = id;
		this.pm.set(pm);
		this.pr.set(pr);
		this.pt.set(pt);
		this.stepSize.set(stepsize);
		this.interactionRadius.set(interactionRadius);
	}
	
	
	/**
	 * Aktualiesiert die aktuelle Richtung und Position des Agenten.
	 * Zus�tzlich werden die gesch�tzten Wahrscheinlichkeiten berechnet.
	 */
	public void update() {
		this.handleStateData();
		this.handleHeading();
		this.handlePosition();
	}
	
	
	/**
	 * Aktualiesiert die aktuelle Richtung und Position des Agenten,
	 * w�rend der Initialisierungsphase der Simulation.
	 */
	public void goThroughInitialSteps() {
		this.handleHeading();
		this.handlePosition();
	}

	
	/**
	 * Setzt das Bewegungsmodell des Agenten zur�ck.
	 */
	public void reset() {
		this.changesOfTurnsCounter = 0;
		this.updatesCounter = 0;
		this.stateMap.clear();
		this.curPm.set(0);
		this.curPr.set(0);
		this.curPt.set(0);
	}
	
	
	/**
	 * Setzt die aktelle Position im Bezug auf die Einteilung der Welt in
	 * Segmente und Spuren.
	 * @param segment - index des Segmentes.
	 * @param track - index der Spur.
	 */
	public void setSectorPosition(int segment, int track) {
		this.sectorPosition.setPosition(segment, track);
	}
	
	
	/**
	 * Gibt die Position im Bezug auf die Einteilung der Welt in
	 * Segmente und Spuren zur�ck.
	 * @return Position(Segment,Spur) auf der Welt
	 */
	public Position getSectorPosition() {
		return this.sectorPosition;
	}
	
	
	/**
	 * Bestimmt die zurzeit im Interaktionsradius befindlichen Agenten.
	 * @param agents - Agenten die sich im Interaktionsradius befinden.
	 */
	public void setInteractions(Agent[] agents) {
		this.interactedAgents = agents;
	}

	
	/**
	 * Gibt die im Interaktionsradius befindlichen Agenten als Array zur�ck.
	 * @return Agenten die sich im Interaktionsradius befinden.
	 */
	public Agent[] getInteractedAgents() {
		return this.interactedAgents;
	}

	
	/**
	 * Gibt zur�ck ob sich kein Agent im Interaktionsradius befindet.
	 * @return true, wenn der Agent allein ist, sonst false.
	 */
	public boolean isAlone() {
		return this.interactedAgents.length == 0;
	}

	
	/**
	 * Gibt die aktuelle Richtung des Agenten in Radiant zur�ck.
	 * @return aktuelle Richtung des Agenten.
	 */
	public double getHeading() {
		return this.heading;
	}
	
	
	/**
	 * Setzt die aktuelle Richtung des Agenten.
	 * @param heading - Richtung in Radiant.
	 */
	public void setHeading(double heading) {
		this.heading = heading;
	}
	
	
	/**
	 * Variable die, die derzeitige Wahrscheinlichkeit einer Bewegung angibt.
	 * @return derzeitige Wahrscheinlichkeit einer Bewegung.
	 */
	public ReadOnlyDoubleProperty curPmProperty() {
		return this.curPm;
	}
	
	
	/**
	 * Variable die, die derzeitige Wahrscheinlichkeit zu verweilen angibt.
	 * @return derzeitige Wahrscheinlichkeit zu verweilen.
	 */
	public ReadOnlyDoubleProperty curPrProperty() {
		return this.curPr;
	}
	
	
	/**
	 * Variable die, die derzeitige Wahrscheinlichkeit die Richtung zu �ndern angibt.
	 * @return derzeitige Wahrscheinlichkeit die Richtung zu �ndern.
	 */
	public ReadOnlyDoubleProperty curPtProperty() {
		return this.curPt;
	}
	
	
	/**
	 * Variable die, die voreingestellte Wahrscheinlichkeit einer Bewegung angibt.
	 * @return eingestellte Wahrscheinlichkeit einer Bewegung.
	 */
	public DoubleProperty pmProperty() {
		return this.pm;
	}
	
	
	/**
	 * Variable die, die voreingestellte Wahrscheinlichkeit zu verweilen angibt.
	 * @return eingestellte Wahrscheinlichkeit zu verweilen.
	 */
	public DoubleProperty prProperty() {
		return this.pr;
	}
	
	
	/**
	 * Variable die, die eingestellte Wahrscheinlichkeit die Richtung zu �ndern angibt.
	 * @return eingestellte Wahrscheinlichkeit die Richtung zu �ndern.
	 */
	public DoubleProperty ptProperty() {
		return this.pt;
	}
	
	
	/**
	 * Variable die, die eingestellte Schrittweite angibt.
	 * @return eingestellte Schrittweite.
	 */
	public DoubleProperty stepSizeProperty() {
		return this.stepSize;
	}

	
	/**
	 * Variable, die den eingestellte Interaktionsradius angibt.
	 * @return eingestellter Interaktionsradius.
	 */
	public DoubleProperty interactionRadiusProperty() {
		return this.interactionRadius;
	}

	
	/**
	 * Gibt die Id des Agenten zur�ck
	 * @return Id des Agenten
	 */
	public int getId() {
		return this.id;
	}
	
	
	/**
	 * Gibt die Position des Agenten zur�ck
	 * @return Position des Agenten
	 */
	public Position getPosition() {
		return this.position;
	}
	
	
	/**
	 * Setzt die Position des Agenten auf die �bergebenen Koordinaten.
	 * @param x - neue X-Koordinate des Agenten.
	 * @param y - neue Y-Koordinate des Agenten.
	 */
	public void setPosition(double x, double y) {
		this.getPosition().setPosition(x, y);
	}

	
	/**
	 * Gibt die Distanz zwischen zwei Agenten zur�ck
	 * @param other Agent zu dem gemessen wird.
	 * @return Distanz zwischen zwei Agenten.
	 */
	public double distanceTo(Agent other) {
		return this.getPosition().distanceTo(other.getPosition());
	}

	
	/**
	 * Gibt den Interaktionsradius des Agenten zur�ck
	 * @return Interaktionsradius des Agenten
	 */
	public final double getInteractionRadius() {
		return this.interactionRadius.get();
	}

	
	/**
	 * Pr�ft ob ein Agent mit einem anderen Agent in interaktion ist.
	 * @param agent - Agent der gepr�ft wird.
	 * @return true, wenn eine Interaktions vorliegt, sonst false.
	 */
	public boolean interactWith(Agent agent) {
		if(this != agent && this.position.distanceTo(agent.getPosition()) <= this.getInteractionRadius()) {
			return true;
		}
		return false;
	}

	
	/**
	 * Bestimmt die derzeitigen Wahrscheinlichkeiten (curPm,curPr,curPt) des Agenten.
	 */
	private void handleStateData() {
		this.updatesCounter++;
		Map<State, Integer> map = this.stateMap.getOrDefault(this.preState, new HashMap<>());
		map.put(this.curState, map.getOrDefault(this.curState, 0) + 1);
		this.stateMap.put(this.preState, map);
		
		// Anhand einer Markovkette ersten Grades werden die Wahrscheinlichkeiten Pm und Pr gepr�ft.
		this.stateMap.forEach((rowState, column) -> {
			column.forEach((columnState, integer) -> {
				if(rowState == columnState) {
					double sum = column.values().stream().reduce(0, (a, b) -> a + b);
					double value = Math.round((integer / sum) * 100.0) / 100.0;
					
					if(rowState == State.MOVE) {
						this.curPm.set(value);
					}
					if(rowState == State.REST) {
						this.curPr.set(value);
					}
				}
			});
		});
		this.curPt.set(Math.round(((double)this.changesOfTurnsCounter / this.updatesCounter) * 100.0) / 100.0);
	}

	
	/**
	 * Bestimmt mit einer Wahrscheinlichkeit (Pt) eine neue Richtung.
	 */
	private void handleHeading() {
		double probability = MathHelpers.random.nextDouble();
		if(probability < this.pt.get()) {
			this.heading = this.calcNewHeading();
			this.changesOfTurnsCounter++;
		}
	}
	
	
	/**
	 * Bestimmt anhand der Wahrscheinlichkeiten(Pm, Pr), in welchem Zustand
	 * sich das Bewegungsmodell befindet.
	 */
	private void handlePosition() {
		double probability = MathHelpers.random.nextDouble();
		this.preState = this.curState;
		switch (this.curState) {
			case MOVE:
				if(probability > this.pm.get()) {
					this.curState = State.REST;
				}
				this.move();
				break;
			case REST:
				if(probability > this.pr.get()) {
					this.curState = State.MOVE;
				}
				break;
		}
	}

	
	/**
	 * Errechnet abh�ngig von der aktuellen Richtung, eine neue.
	 * Die Richtung wird anhand der von Mises Verteilung (Normalverteilung auf einem Kreis)
	 * bestimmt.
	 * @return neue Richtung in Radiant.
	 */
	private double calcNewHeading() {
		return this.heading + MathHelpers.vonMisesDistribution();
	}
	
	
	/**
	 * Berrechnet eine neue Position abh�ngig von der �bergebenen Richtung und
	 * der Schrittweite.
	 * @param heading - Richtung in welche die neue Position berechnet wird.
	 * @return neue Position
	 */
	private Position calcNewPosition(double heading) {
		return new Position(
			this.position.getFirst() + this.stepSize.get() * Math.cos(heading),
			this.position.getSecond() + this.stepSize.get() * Math.sin(heading)
		);
	}

	
	/**
	 * Berrechnet eine neue Position die sich innerhalb der Welt befindet.
	 */
	private void move() {
		double newHeading = this.heading;
		Position newPosition = this.calcNewPosition(newHeading);
		
		// Solange die neue Position au�erhalb der Welt liegt.
		// Verhalten im Randbereich.
		while(this.isOutOfBounds(newPosition)) {
			newHeading = this.calcNewHeading();
			newPosition = this.calcNewPosition(newHeading);
			if(newPosition.distanceTo(World.center) < this.position.distanceTo(World.center)) {
				this.heading = newHeading;
			}
		}
		this.position.setPosition(newPosition);
	}
	
	
	/**
	 * Pr�ft ob sich die �bergebene Position innerhalb der Welt befindet.
	 * @param position - Position, welche gepr�ft werden soll.
	 * @return true wenn sich die Position innerhalb der Welt befindet, sonst false
	 */
	private boolean isOutOfBounds(Position position) {
		if(position.distanceTo(World.center) > World.center.getFirst()) {
			return true;
		}
		return false;
	}
}
